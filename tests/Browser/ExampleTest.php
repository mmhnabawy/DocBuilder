<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class ExampleTest extends DuskTestCase
{
    /**
     * A basic browser test example.
     *
     * @return void
     */
    public function testLogin()
    {
        // $this->browse(function (Browser $browser) {
        
        //for type method, If a CSS selector is not provided, Dusk will search for an input field with the given name attribute


        //     $browser->visit('/login')
        //             ->type('email', 'mm_h434@yahoo.com')
        //             ->type('password', '123456')
        //             ->press('Login')
        //             ->assertPathIs('/home');
       
        //     $browser->visit('/')
        //             ->assertSee('Laravel5');


         //$browser->clickLink($linkText);//This method interacts with jQuery. If jQuery is not available on the page, Dusk will automatically inject it into the page so it is available for the test's duration.

        // Retrieve the value...
        //$value = $browser->value('selector');

        // Set the value...
        //$browser->value('selector', 'value');
        // Set the text...
        //$text = $browser->text('selector');
        //to retrieve an attribute of an element matching the given selector
        //$attribute = $browser->attribute('selector', 'value');

        //To append text to a field without clearing its content, you may use the append method:
        //$browser->type('tags', 'foo')
        //    ->append('tags', ', bar, baz');

        //You may clear the value of an input using the clear method
        //$browser->clear('email');

        //dropdowns: When passing a value to the  select method, you should pass the underlying option value instead of the display text. You may select a random option by omitting the second parameter
        //$browser->select('size', 'Large');

        //checkboxes
        //$browser->check('terms'); $browser->uncheck('terms');
        
        //radiobuttons: if an exact css selector match can't be found, Dusk will search for a radio with matching name and value attributes
        //$browser->radio('version', 'php7');

        //attach files
        //$browser->attach('photo', __DIR__.'/photos/me.png');

        //advanced type using shift(modifier keys)
        //$browser->keys('selector', ['{shift}', 'taylor'], 'otwell');//type 'taylor' when shift is held

        //mouse control
        //$browser->click('.selector');
        //browser->mouseover('.selector');
        //$browser->drag('.from-selector', '.to-selector');
        //$browser->dragLeft('.selector', 10);

        //assert that some text exists only within a table and then click a button within that table
        /*$browser->with('.table', function ($table) {
            $table->assertSee('Hello World')
            ->clickLink('Delete');
        });*/

        // pausing the test to wait for an element to appear
        //$browser->pause(1000);

    
        // Wait a maximum of five seconds for the selector to appear
        //$browser->waitFor('.selector');
        // Wait a maximum of five seconds for the selector to appear and do something with it,All element operations performed within the given callback will be scoped to the original selector
        //$browser->whenAvailable('.modal', function ($modal) {
        //$modal->assertSee('Hello World')
        //    ->press('OK');
        //});

        // Wait a maximum of one second for the selector...
        //$browser->waitFor('.selector', 1);

        // wait until the given selector is missing from the page
        //$browser->waitUntilMissing('.selector');

        //When making a path assertion such as $browser->assertPathIs('/home'), the assertion can fail if window.location.pathname is being updated asynchronously. You may use the  waitForLocation method to wait for the location to be a given value
        //$browser->waitForLocation('/secret');
        
        //If you need to make assertions after a page has been reloaded
        //$browser->click('.some-action')
        //->waitForReload()
        //->assertSee('something');

        // });



    }

       public function testFakingWithLogin()
    {

         $user = factory(\App\User::class)->create([
            'name' =>'misho2'
        ]);

        $this->browse(function ($browser) use ($user) {
            //$browser->resize(1920, 1080);
            //$browser->maximize();
            $browser->visit('/login')
                    ->type('email', $user->email)
                    ->type('password', 'secret')
                    ->press('Login')
                    ->assertPathIs('/DocBuilder/public/home');// it adds the domain name only
        });

    }


    public function testMultipleBrowsers()
    {//loginAs method accepts a user ID or user model instance
        $this->browse(function ($first, $second) {
        $first->loginAs(User::find(1))
              ->visit('/home')
              ->waitForText('Message');

        $second->loginAs(User::find(2))
               ->visit('/home')
               ->waitForText('Message')
               ->type('message', 'Hey Taylor')
               ->press('Send');

        $first->waitForText('Hey Taylor')
              ->assertSee('Jeffrey Way');
        });
    }
}
