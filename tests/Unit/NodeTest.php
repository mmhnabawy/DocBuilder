<?php
namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Http\Controllers\NodesController;
use PDO;
class NodeTest extends TestCase
{

// only instantiate pdo once for test clean-up/fixture load
    static private $pdo = null;

    // only instantiate PHPUnit_Extensions_Database_DB_IDatabaseConnection once per test
    private $conn = null;

	public function setUp()
	{
		$this->getConnection(null);
        self::$pdo->exec("BEGIN");
		$this->node = new NodesController();
	}

	public function tearDown()
	{
		 self::$pdo->exec("ROLLBACK");
	}

    public function getConnection($connection= null)
    {
        if ($this->conn === null) {
            if (self::$pdo == null) {
                self::$pdo = new PDO( $GLOBALS['DB_DSN'], $GLOBALS['DB_USER'], $GLOBALS['DB_PASSWD'],
                array(PDO::ATTR_PERSISTENT => false) );
            }
            //$this->conn = $this->createDefaultDBConnection(self::$pdo, $GLOBALS['DB_DBNAME']);
        }

        return $this->conn;
    }

    public function testDB()
    {
        self::$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        //self::$pdo->beginTransaction();
        self::$pdo->exec("insert into docs (id, name, user_id) values (22, 'Joe', 2)");
        //self::$pdo->exec("delete from docs (id, name, user_id) values (22, 'Joe', 2)");
        //self::$pdo->rollBack();
        $this->assertEquals(1,1);
    } 

	/**
     * A basic test example.
     *
     * @return void
     */
    // public function testExample()
    // {
    // 	$expected = 5;
    // 	$actual = $this->node->index($expected);
    // 	$this->assertEquals($expected, $actual, 'input and output must be equal' );
    //     //$this->assertTrue(true);
    // }

}