<?php

namespace Tests;

use Laravel\Dusk\TestCase as BaseTestCase;
//use Facebook\WebDriver\Chrome\ChromeOptions;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Appstract\DuskDrivers\Opera\SupportsOpera;

abstract class DuskTestCase extends BaseTestCase
{
    use CreatesApplication, SupportsOpera;

    /**
     * Prepare for Dusk test execution.
     *
     * @beforeClass
     * @return void
     */
    public static function prepare() //uses info supplied in driver function
    {
        //static::startChromeDriver();
        static::startOperaDriver();
    }

    /**
     * Create the RemoteWebDriver instance.
     *
     * @return \Facebook\WebDriver\Remote\RemoteWebDriver
     */
    protected function driver() // joint between web driver interface and the browser interface
    {
       /* $options = (new ChromeOptions)->addArguments([
            'binary',
            'C:/Program Files (x86)/Opera/launcher.exe'
        ]);*/
        return RemoteWebDriver::create(
            'http://localhost:9515', ["browserName"=>"opera","chromeOptions"=>["args"=>[],"extensions"=>[],"binary"=>"C:/Program Files (x86)/Opera/launcher.exe"]] );

       /* return RemoteWebDriver::create(
            'http://localhost:9515', [DesiredCapabilities::opera(),'binary' => 'C:/Program Files (x86)/Opera/launcher.exe']
        );*/
    }
}
