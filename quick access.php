<!DOCTYPE html>
<html>
<head>
     <meta name="viewport" content="width=device-width, initial-scale=1">
	<title></title>
<style type="text/css">
.bubble{
border-radius:50%;
background-color: #F0AD4E;
width:70px;
height:70px;
text-align: center;
border: 0px;
cursor:pointer;
display: inline-block;
animation: train_flow;
animation-duration: 0.7s;
animation-fill-mode: initial;
color: brown;
}
.bubble:hover{
transition:0.5s all;
transform:scale(1.2);
}
.bubble a{
    line-height: 60px;
}
#container2{
display:none;
width:100px;
height: 75%;
background-color:#EBEDEF;
border-radius:5px;
overflow-y:auto;
position:fixed;
right:30px;
z-index: 0;
border:2px solid #641c94;
margin-top: 10px;
animation: none;
animation-duration: 1s;
animation-fill-mode: initial;
}

.container3{
position: relative;
width: 70px;
height: 70px;
display: block;
float:left;
margin-left:20px;
margin-top:20px;
}

.container3 .bubble a{
text-decoration: none;
color: #4285F4;
}

.container3 .bubble p{
color :white;
text-align: center;
margin: auto;
margin-top:30px;
width:80%;
word-break: break-all;
white-space:normal;
height:50%;
}
.quick-icon a:hover{

}
</style>
</head>
<body>
	
<div id="container2" >
<?php for ($i=0; $i < 5 ; $i++) {   ?>
    <div class="container3">
        <div class="bubble" onclick="animateTopBubble(90);">
            <a href=""> Hello <?= $i?></a>
           
        </div>
    </div>
<?php } ?>

    <a href="" class="btn btn-info" style="position:fixed;bottom:15px;right:15px;height:100px;padding-top:40px;">
        <span class="glyphicon glyphicon-plus"></span>
    </a>
</div>

<div class="quick-icon" style="position:fixed;right:50px;bottom:50px;width:70px;height:70px;border-radius:50%;">
    <a onmouseover="changeSrc(0)" onmouseleave="changeSrc(1)" onclick="$('#container2').slideToggle();" href="javascript:;"><img id="icon-img" src="/DocBuilder/public/images/worldwide.png" style="width:70px;height:70px;"></a>
</div>

</body>
</html>
<script type="text/javascript" src="/Shipping/js/jquery-3.2.1.min.js"></script>

<script type="text/javascript">
  function changeSrc(input)
  {
    if(input == 0)
    {
        document.getElementById('icon-img').src = "/DocBuilder/public/images/worldwide_2.png";
    }
    else
    {
document.getElementById('icon-img').src = "/DocBuilder/public/images/worldwide.png";

    }
  }

document.addEventListener('click',function(e){
    //$('#container2').fadeOut();
});

</script>