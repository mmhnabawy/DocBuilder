<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DocsPermission extends Model
{
   protected $fillable = ['doc_id', 'start_node_id', 'user_id','giver_id', 'permission', 'new', 'recursive'] ;
  public $timestamps = false;
}
