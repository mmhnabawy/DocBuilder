<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contributer extends Model
{
    protected $fillable = ['team_id', 'user_id'];
    public $timestamps = false;

    /**
     * A user can have many messages
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function messages()
    {
      return $this->belongsTo(Team::class);
    }
}
