<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Validator;

class ChangePasswordController extends Controller
{

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

/**
 * Get a validator for an incoming registration request.
 *
 * @param  array  $data
 * @return \Illuminate\Contracts\Validation\Validator
 */
    protected function change_form()
    {
        return view('auth.passwords.change');
    }

    public function admin_credential_rules(array $data)
    {
        $messages = [
            'old-password.required' => 'Please enter the old password',
            'new-password.required' => 'Please enter the new password',
        ];

        $validator = Validator::make($data, [
            'old-password'      => 'required',
            'new-password'      => 'required|same:new-password',
            'password_confirmation' => 'required|same:new-password',
        ], $messages);

        return $validator;
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function change(Request $request)
    {       //dd($request->all());
        if (\Auth::Check()) {

            $request_data = $request->all();
            $validator    = $this->admin_credential_rules($request_data);

            if ($validator->fails()) {
                dd($validator);
                return response()->json(array('error' => $validator->getMessageBag()->toArray()), 400);

            } else {
                $current_password = \Auth::User()->password;
                if (\Hash::check($request_data['old-password'], $current_password)) {
                    $user_id            = \Auth::User()->id;
                    $obj_user           = User::find($user_id);
                    $obj_user->password = \Hash::make($request_data['new-password']);
                    $obj_user->save();
                    return redirect()->back()->with('status', 'success');;
                } else {
                    $error = array('old-password' => 'Please enter correct current password');
                    return response()->json(array('error' => $error), 400);
                }
            }
        } else {
            return redirect()->to('/');
        }

    }
}
