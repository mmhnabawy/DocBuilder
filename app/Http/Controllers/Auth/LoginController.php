<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
     */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Redirect the user to the facebook authentication server.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider() //redirecting the user to the OAuth provider
    {
        return \Socialite::driver('facebook')->redirect();
    }

    /**
     * Obtain the user information from facebook.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback() // receiving the callback from the provider after authentication
    {
        try {

            $user = \Socialite::driver('facebook')->user();
        } catch (\Exception $e) {
            return redirect('/home');
        }
        //check if user exists
        $exists = User::where('facebook_id', $user->getId())->first();
        if (!$exists) {
            $exists = User::create([
                'email'       => $user->getEmail(),
                'facebook_id' => $user->getId(),
                'name'        => $user->getName(),
                'password'    => 'Nosdad1aad',
                'phone'       => '01016415791',
                'gender'      => strtolower($user->user['gender'][0]),
            ]);
        }

        // object to array
        \Auth::login($exists);

        return redirect()->to('/');
        // $user->token;
    }

}
