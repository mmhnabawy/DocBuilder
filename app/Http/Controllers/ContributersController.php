<?php

namespace App\Http\Controllers;

use App\{User, Contributer};// it's in the global namespace as it's a facade
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Response;

class ContributersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //                  my teams
        $teams =\DB::table('teams')
            ->join('contributers', 'contributers.team_id', '=', 'teams.id')
            ->groupBy('teams.id')
            ->where(['leader_id'=>\Auth::id()])
            ->select(\DB::raw('count(contributers.id) as members, (select count(meeting_issues.id)  from meeting_issues where team_id=teams.id) as issues , teams.id, teams.name'))
            ->get();
           
        //                 my teams members
        $contributers = \DB::table('contributers')
            ->join('teams', 'contributers.team_id', '=', 'teams.id')
            ->join('users', 'contributers.user_id', '=', 'users.id')
            ->select('users.id','users.name', 'users.email', 'teams.name as team')
            ->where('teams.leader_id', '=', \Auth::user()->id)
            ->where('contributers.user_id', '<>', \Auth::user()->id)
            ->get();

        //                  my assignments

         $assigns = \DB::table('docs_permissions')
            ->join('users', 'docs_permissions.user_id', '=', 'users.id')->join('nodes','docs_permissions.start_node_id','=','nodes.id' )
            ->select('users.id as user_id','users.name as user_name','nodes.name as node_name','docs_permissions.permission' )
            ->where('docs_permissions.giver_id', '=', \Auth::user()->id)
            ->get();

            foreach ($assigns as $key => $value) {
                   // 0 => admin, 1 => editor

              switch ($value->permission) {
                  case 1:
                      $value->permission = 'Admin' ;
                      break;
                   case 2:
                    $value->permission = 'Editor' ;
                      break;

                    case 3:
                    $value->permission = 'Reviwer' ;
                      break;

                  default:
                      # code...
                      break;
              }
          
            }

        //                  my contributions
        $myContributions = \DB::table('docs_permissions')
            ->join('docs', 'docs_permissions.doc_id', '=', 'docs.id')
            ->join('nodes', 'docs_permissions.start_node_id', '=', 'nodes.id')
            ->select('docs.name as doc_name', 'nodes.name as node_name', 'nodes.id as node_id', 'level', 'permission')
            ->where('docs_permissions.user_id', '=', \Auth::user()->id)
            ->get();

        //update new flag in my contributions
        \DB::table('docs_permissions')->where('user_id', \Auth::user()->id)->update(['new' => '0']);


        foreach ($myContributions as $key => $value) {
            // give role names
            if($value->permission == 0)
                $value->permission = 'Admin';
            //add next level for showing
            $value->nextLevel = $value->level + 1 ;
        }


       
        return view('Contributers.index', compact('teams','contributers', 'assigns','myContributions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        //check if its our form aginst csrf
        if (\Session::token() !== Input::get('_token')) {
            return Response::json(array(// http response with 403
                'status' => '4','msg' => 'Unauthorized attempt'
            ));
        }
            
            // $user = User::where( ['email'=> Input::get( 'email' )])->get();  // try

        $id = \DB::table('users')->where('email', trim(Input::get('email')))->pluck('id');

        $team_id = Input::get('team');
            // ensure that the added user exists in users table 
        if(count($id) === 0)
            return Response::json(array(
            'status' => '2',
        ));

        $contributer_id = $id[0];
        
        //ensure entry not exist
        //get team id 
        $exist_count = Contributer::where(['team_id'=> $team_id, 'user_id'=> $contributer_id ])->count();
        if ($exist_count > 0) {
            return Response::json(array(// http response with 403
                'status' => '1'
            ));
        }

        //ensure added user not the logged in user
        if (\Auth::id() === $contributer_id) {
            return Response::json(array(// http response with 403
            'status' => '4'
            ));
            //DocsPermission::create($entry);
        }

        // if all previous conditions are satisfied
         $entry = ['team_id' => $team_id,
                'user_id'    => $contributer_id,
            ];

         //ensure permission and he doesn't assign the owner of the doc  ^_^ 
        Contributer::create($entry);

        return Response::json(array(
            'status' => '0',
        ));     
      
    }
      

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
