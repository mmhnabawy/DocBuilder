<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\{OpenNode, User};
class OpenNodesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * check Existence of resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public static function checkExistence($node_id, $user_id)
    {
        // if there is no record for the user with hat node
        return OpenNode::where(['node_id'=>$node_id, 'user_id'=>$user_id])->count();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public static function storeState($node_id, $user_id)
    {
        // if there is no record for the user with hat node
        OpenNode::create(['node_id'=>$node_id, 'user_id'=>$user_id, 'open'=>'1']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public static function showUsersEditingNode($id)
    {
        $users_ids = OpenNode::where(['node_id'=>$id, 'open'=>'1'])->get(['user_id']);
        //dd($users_ids);
        $users = User::whereIn('id',$users_ids)->get(['id','name','photo']);

        return $users;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public static function updateState($node_id, $user_id, $state)
    {
        OpenNode::where(['node_id'=>$node_id, 'user_id'=>$user_id])->update(['open'=> $state]);
    }


}
