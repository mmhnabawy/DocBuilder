<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\{NodesLog, NodesReview};

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function estimatePassedFromPast($past)
    {
        $now  = new \DateTime();
        $past = new \DateTime($past);
        $diff = $now->diff($past);
        //echo $diff->days;
        if ($diff->days == 0) {
            return ("today");
        } else if (0 < $diff->days && $diff->days < 30) {
            return $diff->format("from " . $diff->days . " day(s)");
        } else {
            return "on " . $past->format("Y-m-d");
        }
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $commits = NodesLog::where(['user_id'=>\Auth::user()->id])->orderby('time', 'DESC')->limit(7)->get();
        $reviews = NodesReview::where(['user_id'=>\Auth::user()->id])->orderby('time', 'DESC')->limit(7)->get();
        foreach ($commits as $key => $commit) {
            $commit->time = $this->estimatePassedFromPast($commit->time);
        }

        foreach ($reviews as $key => $review) {
            $review->time = $this->estimatePassedFromPast($review->time);
        }
        return view('home', compact('commits','reviews'));
    }
}
