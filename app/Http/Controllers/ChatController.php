<?php

namespace App\Http\Controllers;

use App\{Message, Contributer};
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Events\MessageSent;
// Since our chat app is an authenticated-only app, we create a private channel called Chat, which only authenticated users will be able to connect to. Using the PrivateChannel class, Laravel is smart enough to know that we are creating a private channel, so don't prefix the channel name with private- (as specified by Pusher), Laravel will add the private- prefix under the hood.

class ChatController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

/**
 * Show chats
 *
 * @return \Illuminate\Http\Response
 */
    public function index($team_id)
    {
      
      return view('chat');
    }

/**
 * Fetch all messages
 *
 * @return Message
 */
    public function fetchMessages($team_id)
    {    
      return \DB::table('messages')
              ->join('users', 'users.id', '=', 'messages.user_id')
              ->where(['team_id'=>$team_id])
              ->select(\DB::raw('messages.message, messages.user_id, users.name'))
              ->orderBy('messages.created_at','DESC')
              ->limit(20)
              ->get();
      //return Message::where('team_id', $team_id)->with(['name', 'id'])->orderBy('created_at','ASC')->pluck('message');
    }

//remember to use
// use App\Events\MessageSent;

  /**
   * Persist message to database
   *
   * @param  Request $request
   * @return Response
   */
  public function sendMessage(Request $request)
  {
    $user = Auth::user();
    //send to team
    $team_id = $request->input('team_id');
    $message = $user->messages()->create([
      'message' => $request->input('message'),
      'team_id' => $team_id
    ]);
    
    broadcast(new MessageSent($user, $message, $team_id));
    //broadcast( new MessageSent($user, $message, $team_id))->toOthers();//exclude the current user from the broadcast's recipients.
    return ['status' => 'Message Sent!'];
  }

}