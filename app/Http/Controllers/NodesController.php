<?php

namespace App\Http\Controllers;

use App\Http\Controllers\{NodesLogsController, OpenNodesController, PermissionsController};
use App\{Doc, Node, NodesLog};
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Response;

class NodesController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($test_id)
    {
        return $test_id;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($doc_id, $level, $parent_id)
    {
        if($level < 20)
        {
            $info = array('doc' => $doc_id, 'level' => $level, 'parent_id' => $parent_id);
            return view('Nodes.create', compact('info'));
        }else{
            $limit_message = "nodes exceded 20 levels";
             return view('Payment.index', compact('limit_message'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $summary = "initial commit";
        $input = $request->all();
        //for show action
        $parent_id = $input['parent_id'];

        if ($input['level'] == 1) {
            $input['parent_id'] = null;
        }

        $node = Node::create($input);

        // create initial commit
        $logsController = new NodesLogsController();
        $changes  = $logsController->detectNodeChanges($input['content'], "");
        // first log has null parent(older)
        $log_id = $logsController->store($node['id'], $summary, $changes);

        // update last_commited_log for the node
        Node::findOrFail($node->id)->update(['last_commited_log'=> $log_id]);
        //\DB::table('nodes')->where(['id'=> $id])->update(['last_commited_log'=> $log_id]);

        return redirect()->action(
            'NodesController@show', ['level' => $input['level'], 'id' => $parent_id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($level, $id) // parent node id (doc or node depending on the level)
    {   
        if ($level == 1) {
            //parent will be the doc
            if(PermissionsController::checkAccessForDoc($id)){
                $parent         = Doc::findOrFail($id); //doc
                $children_nodes = Node::where(['doc_id' => $id, 'level' => 1])->get();
            }else{
                return response("<h1 style='color:red'>Permission Denied Access !<h1>", 403);
            }

        } else {
            
            $parent = Node::findOrFail($id);
            if(PermissionsController::checkAccessForNode($parent, [1,2,3])) {
                
                $children_nodes = Node::where(['parent_id' => $id])->get();

            }else{

                return response("<h1 style='color:red'>Permission Denied Access !<h1>", 403);
            }
            //$children_nodes->pull($parent);
        }
        // hint: store it in session and update whenever on contributions page

        //exclude owner from list
        $ownerId = self::getNodeOwner($parent->doc_id);
        //dd($ownerId);
        $contributers = \DB::table('contributers')
            ->join('users', 'contributers.user_id', '=', 'users.id')
            ->select('users.name', 'users.id')
            ->where([['contributers.user_id', '=', \Auth::user()->id], ['users.id','<>', $ownerId]])
            ->get();

        return view('Nodes.show', compact('children_nodes',
            'parent', 'level', 'contributers'));
    }

    public function getNodeOwner($doc_id)
    {
        $owner = Doc::where(['id'=>$doc_id])->get(['user_id']);
        if(count($owner) > 0)
           return  $owner[0]['user_id'];
        return 0;
    }

    public function constructParents($logs)//makes an array with a parent id as the key and value are their children.. no nesting more than that
    {
        // define key for each parent but starting here with the root
        $root = $logs[0] ;
        $newlogs = array($root['id'] => []);
        
        //initialize
        foreach ($logs as $key => $log) {
            $newlogs[$log['id']] = [];
        }

        foreach ($logs as $key => $log) {
            $newlogs[$log['older_log']][] = $log;
        }

        //cleaning the new logs from empty keys
        foreach ($newlogs as $key => $value) {
            if (empty($value)) {
                unset($newlogs[$key]);
            }
        }

        return $newlogs;
    }

    public function constructRootChildren(string $id, $input_array, $last_commited_log)
    {
        //stoping condition
        if (!isset($input_array[$id])) { //meaning if count($children) == 0 but because of undefined index notice, use another check
            //secondary stopping condition in php
            return;
        }

        $children = $input_array[$id];
        $tag = "";

        if (count($children) == 1) {
            //generate li for the child
            $elem = $children[0];
            $class = $elem['id'] == $last_commited_log ? 'selectedState' : '';
            $time = $this->estimatePassedFromPast($elem['time']) ;
            $tag .= "<li class='{$class}' data-changeId='{$elem['id']}' style='cursor:pointer;'>{$elem['summary']} {$time}";
            if (isset($input_array[$elem['id']])) {
            // primary stopping in php
                $tag .= $this->constructRootChildren($elem['id'], $input_array, $last_commited_log);
            }
            $tag .= "</li>";

        } else if (count($children) > 1) {
            //children check
            //generate ul foreach child
            foreach ($children as $key => $elem) {
                $class= $elem['id'] == $last_commited_log ? 'selectedState' : '';
                $time = $this->estimatePassedFromPast($elem['time']) ;
                $left= -170+($key * 120);
                $tag .= "<ul style='list-style:disc;display:inline-block;position:relative;top:80px;left:{$left}px;'><li style='cursor:pointer;' class='{$class}' data-changeId='{$elem['id']}'>{$elem['summary']}  {$time}";
                if (isset($input_array[$elem['id']])) {
                    $tag .= $this->constructRootChildren($elem['id'], $input_array, $last_commited_log);
                }
                $tag .= "</li></ul>";
            }
        }

        return $tag;
    }

    public function constructRoot(&$logs, $last_commited_log)
    {
        $root      = $logs[0];
        $logs      = $this->constructParents($logs);
        $time = $this->estimatePassedFromPast($root['time']) ;
        $class = $root['id'] == $last_commited_log ? 'selectedState' : '';
        $logs_view = "<li class='{$class}' style='cursor:pointer;' data-changeId='{$root['id']}'>{$root['summary']}  {$time}";
        $logs_view .= $this->constructRootChildren($root['id'], $logs, $last_commited_log);
        $logs_view .= "</li>";

        return $logs_view;
    }


    public function estimatePassedFromPast($past)
    {
        $now  = new \DateTime();
        $past = new \DateTime($past);
        $diff = $now->diff($past);
        //echo $diff->days;
        if ($diff->days == 0) {
            return ("today");
        } else if (0 < $diff->days && $diff->days < 30) {
            return $diff->format("from " . $diff->days . " day(s)");
        } else {
            return "on " . $past->format("Y-m-d");
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   //get current node info
        $node = Node::findOrFail($id);
        if(!empty($node)) {
            if(PermissionsController::checkAccessForNode($node, [1,2,3])) {
                //check access through parent node permisssion
                $user_id =\Auth::user()->id;
                //insert entry to the table open nodes with the current user to indicate that he is editing
                if(OpenNodesController::checkExistence($id, $user_id) > 0) {
                   OpenNodesController::updateState($id, $user_id, '1');
                } else {
                    OpenNodesController::storeState($id, $user_id);
                }

                //select all who are currently editing
                $editingUsers = OpenNodesController::showUsersEditingNode($id);

                

                //get its parent in case user wants to go back
                $level  = $node->level;
                $parent = $level == 1 ? $node->doc_id : $node->parent_id;

            $logs  = \DB::table('nodes_logs')
            ->join('users', 'nodes_logs.user_id', '=', 'users.id')
            ->select('users.name', 'users.id as user_id','nodes_logs.id', 'nodes_logs.summary', 'nodes_logs.time')
            ->where('nodes_logs.node_id', '=', $id)->orderBy('nodes_logs.id', 'asc')
            ->get();

                //NodesLog::where(['node_id' => $id])->orderBy('id', 'asc')->get(['id', 'summary', 'time']);

                $logs_count = count($logs);
                foreach ($logs as $key => $log) {
                    $log->time = $this->estimatePassedFromPast($log->time);
                }
                //last log is the parent for the new one
                //$older_log = $node->last_commited_log;
                //$logs_view = $this->constructRoot($logs, $older_log);

                return view('Nodes.edit', compact('node', 'parent', 'logs_count', 'logs', 'editingUsers'));
            }
             return response("<h1 style='color:red'>Permission Denied Access !<h1>", 403);
        }

            return response('Not Found !', 404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) //F->F (from newest node on it's path to a newer one: no forward after reverting >>>>> know from commit submit not undo) and can go from a break point to another path
    {
        $input   = $request->all();
        $summary = $input['message'];
        $node    = Node::findOrFail($id);
        //save logs for reverting in future
        $logsController = new NodesLogsController();
        $changes        = $logsController->detectNodeChanges($input['content'], $node->content);

        //save a log with a new path if user made a commit from a break point in the old path

        //$older_log = $input['olderLog'];
        if (!empty($changes)) {
        //if no change or initial commit, dont save it in db
           $lastCommited = $logsController->store($id, $summary, $changes);
        }

        //update content with the new or last changes and state
        $input['state']   = 'f';
        $input['summary'] = $summary ;
        //save 0 as last commited log's id in nodes table to highlight last one
        $input['last_commited_log'] = $lastCommited ;
        $node->update($input);

        return redirect()->back();

    }


    //              level correcting
    public function pasteCopied($nodeId, $newDocId, $newParentId)
    {
        //1-ensure permissions

        //2-if succeeded, copy it and its children
        if(filter_var($nodeId, FILTER_VALIDATE_INT) && filter_var($nodeId, FILTER_VALIDATE_INT) && filter_var($newParentId , FILTER_VALIDATE_INT)) { 
            $parent = Node::where(['id'=>$nodeId])->get()->toArray()[0];

            if(count($parent) > 0) {
                $oldDocId = $parent['doc_id'];
                $parent['doc_id'] = $newDocId ;
                $oldId = $parent['id'];
                $parent['id'] = null ;

                if($newParentId == 0) {
                    $parent['parent_id'] = null ;
                    $parent['level'] = 1 ; 
                } else {
                    $parent['parent_id'] = $newParentId ;

                    $parent['level'] = Node::where(['id'=>$newParentId])->get(['level'])[0]->level + 1;

                }
                $newParent = Node::create($parent);

               $this->copyRecursively($newParent->id, $oldId, $oldDocId, $newDocId, $newParent->level);
                
            }
       

            return \Response::json(['status'=>'0']);
        }
        
        return \Response::json(['status'=>'1']);

    }

    public function copyRecursively($newParentId, $oldParentId, $oldDocId, $newDocId, $level)
    {
        $children = Node::where(['parent_id'=>$oldParentId, 'doc_id'=>$oldDocId])->get()->toArray();
        $level++;
        //dd($children);
        if(count($children) == 0) {
            return;

        } else {

            foreach ($children as $key => $child) {
                $child['doc_id'] = $newDocId ;
                $child['parent_id'] = $newParentId ;
                $child['level'] = $level ;
                $newNode = Node::create($child);
               $this->copyRecursively($newNode->id, $child['id'],  $oldDocId, $newDocId, $level);

            }
            
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $node = Node::findOrFail($id);
        if(!PermissionsController::checkAccessForNode($node, [1,2]))//1 -> must have admin permission or editor permission
            return response("<h1 style='color:red'>Permission Denied Access !<h1>", 403);

        Node::findOrFail($id)->delete();

        return redirect()->back();
    }


      /**
     * Remove the specified resource from storage using ajax.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyWithAjax(Request $request)
    {
         if (\Session::token() !== Input::get('_token')) {
            return Response::json(array(// http response with 403
                'status' => '4','msg' => 'Unauthorized attempt'
            ));
        }
        $id = Input::get('node_id') ;
        $node = Node::findOrFail($id);
        if(!PermissionsController::checkAccessForNode($node, [1]))//1 -> must have admin permission
            return response("<h1 style='color:red'>Permission Denied Access !<h1>", 403);

        if( Node::findOrFail($id)->delete()){
            return Response::json(array(// http response with 403
                'status' => '0','msg' => 'Successfully deleted the node !'
            ));
          }
    }

}
