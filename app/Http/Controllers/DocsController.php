<?php

namespace App\Http\Controllers;

use App\{Doc, Node, NodesLog};
use App\Http\Controllers\{NodesLogsController,PermissionsController};
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Response;

class DocsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Docs = Doc::where('user_id', \Auth::user()->id)->get();
        return view('Docs.index', compact('Docs'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //check # of docs <= 100
        $count = Doc::where(['user_id'=>\Auth::id()])->count();
        
        if($count < 20) {
            return view('Docs.create');
        }else{
            $limit_message = "You exceded 20 documents";
            return view('Payment.index', compact('limit_message'));
        }
        
    }


     public function export()
    {
        //add a root node ref to the nodes table from docs table to speed the process or use a select query with (parent_id = null , doc_id = current doc id )
    }

    private function createNodesFromLevels($dom, $level, $parent_id, $doc_id)
    {   //1st level nodes parents are zeroes
        # depth first
        $currentNode = $dom;
        //stopping cond = if leaf node = has no children
        if (count($currentNode->childNodes) === 0) {
            return;
        }
        $level++;
        //create node in db named $currentNode->nodeName and its parent is $parent_id;
        // get the reated node id and pass it for the children
        //get content if it's exist
        $nodeValue = trim($currentNode->nodeValue) ;
        $content =  $nodeValue;
        //create a node
        $id = \DB::table('nodes')->insertGetId(
            array('name' => $currentNode->nodeName, 'level' => $level,
                'content'  => $content, 'parent_id'  => $parent_id,'doc_id'=> $doc_id)
        );
        // create initial commit
        $logsController = new NodesLogsController();
        $changes  = $logsController->detectNodeChanges($content, "");
        $summary ="initial commit";
        $log_id = $logsController->store($id, $summary, $changes);
        
        // update last_commited_log for the node
        \DB::table('nodes')->where(['id'=> $id])->update(['last_commited_log'=> $log_id]);

        foreach ($currentNode->childNodes as $childNode) {
            $this->createNodesFromLevels($childNode, $level, $id, $doc_id);
        }

    }

    private function createTemplateForDoc($docId, $tempIndex,$parent_id=null)
    {
        $xmlDoc = new \DOMDocument();
        $tempIndex = (int)$tempIndex;
        
        switch ($tempIndex) {
            case 0: // software doc
                {
                    $xmlDoc->load(asset('templates/software.xml'));
                    $softwareTemp = $xmlDoc->documentElement;
                    $this->createNodesFromLevels($softwareTemp, 0, $parent_id, $docId);
                }
                break;

            default:
                # code...
                break;
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       // dd($request);
        $input            = $request->all();
        $name             = $input['name'];
        $input['user_id'] = \Auth::user()->id;
        $doc_id           = Doc::create($input)['id'];

        if ($input['template'] !== null) {
            $index = $input['template'];
            $this->createTemplateForDoc($doc_id, $index);
        // pass its index or whatever to a fn that handle the creation of nodes for it and the id of the doc created
        }
        
        return redirect()->action('DocsController@index'); //look at routes first, // go to controller action
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $doc   = Doc::findOrFail($id);
        return view('Docs.edit', compact('doc'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
        Doc::findOrFail($id)->update($input);
        //$Docs = Doc::all();
        return \Redirect::route('Docs.index');// go to routes then controller

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {   
        if(!PermissionsController::checkAccessForDoc($id))
            return response("<h1 style='color:red'>Permission Denied Access !<h1>", 403);
            
        Doc::findOrFail($id)->delete();
        return redirect()->back();
    }
     /**
     * Remove the specified resource from storage using ajax.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyWithAjax(Request $request)
    {   

        if (\Session::token() !== Input::get('_token')) {
            return Response::json(array(// http response with 403
                'status' => '4','msg' => 'Unauthorized attempt'
            ));
        }

         $id =  Input::get('doc_id');

        if(!PermissionsController::checkAccessForDoc($id, [1]))//1 -> must have admin permission
            return response("<h1 style='color:red'>Permission Denied Access !<h1>", 403);
          
        if(  Doc::findOrFail($id)->delete()){
            return Response::json(array(// http response with 403
                'status' => '0','msg' => 'Successfully deleted the node !'
            ));
          }
    }


}
