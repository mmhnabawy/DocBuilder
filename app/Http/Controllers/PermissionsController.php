<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\{Doc, Node, DocsPermission};

// ** if a permission is given to a node, so it's up ot the owner to assign that permission recursively or just for that node 
class PermissionsController extends Controller
{

     public function __construct()
    {
        $this->middleware('auth');
    }

    public static function checkAccessForDoc($doc_id, $requiredPermissions=[1])
    {
        //check if he is the owner (doc, node tables)
        $user_id = \Auth::user()->id;
        $docs_ids = Doc::where(['user_id'=>$user_id, 'id'=>$doc_id])->get()->toArray();
       
        if(count($docs_ids) > 0)
            return true;

        //check if he has permission (node, doc_perm table)

        //narrowing search by checking doc id first in permissions table
        $permittedNodes = DocsPermission::where(['user_id'=>$user_id, 'doc_id'=>$doc_id, 'start_node_id'=> 0])->get(['permission']);
        
        if(count($permittedNodes) > 0 )
            return true;


        return false;

    }
    
   public static function checkParentNodesFor($node_id, $parents)
   {
    //check if this node is a child of one of those parents
    //while (1) {
        $parent_id = Node::where(['id'=>$node_id])->get(['parent_id'])[0]->parent_id;
        
        if($parent_id == null)
            return false;
        if(in_array($parent_id, $parents))
            return true;
    //}
    
   }

    public static function checkAccessForNode($node, $requiredPermissions=[1])
    {
        //check if he is the owner (doc, node tables)
        $user_id = \Auth::user()->id;
        $docs_ids = Doc::where(['user_id'=>$user_id])->get(['id'])->toArray();
        $all = [];
        foreach ($docs_ids as $key => $value) {
          $all[] = $value['id'];
        }

        if(in_array($node->doc_id, $all)) {
            return true;
        }

        //check if he has a permission in (node, doc_perm table)

        //narrowing search by checking doc id first
        $permittedNodes = DocsPermission::where(['user_id'=>$user_id, 'doc_id'=>$node->doc_id])->get(['start_node_id','permission','recursive']);
        
        if(count($permittedNodes)==0)
            return false;

        //narrowing search more by checking the edited node if it matches
        //the start_node in permissions
        $nodes_ids = [];
        foreach ($permittedNodes as $key => $permittedNode) {
            
            if($permittedNode['start_node_id'] == $node->id && in_array($permittedNode['permission'], $requiredPermissions) )
                return true;

            if($permittedNode['recursive'] == 1)// if it's a recursive permission
                $nodes_ids[] = $permittedNode['start_node_id'];

        }   

        //narrowing search more by checking to be edited node's parent 
        return self::checkParentNodesFor($node->id, $nodes_ids);

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
             // 0 => admin, 1 => editor
        // foreach ($contributers as $key => $value) {
        //       switch ($value->permission) {
        //           case 0:
        //               $value->permission = 'Admin' ;
        //               break;
        //            case 1:
        //             $value->permission = 'Editor' ;
        //               break;

        //           default:
        //               # code...
        //               break;
        //       }
        //   }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //ensure unrepeated entry here, check mark in front and disabled
         if (\Session::token() !== Input::get('_token')) {
            return \Response::json(array(// http response with 403
                'status' => '4','msg' => 'Unauthorized attempt'
            ));
        }
        $node_id = Input::get('node_id');
         $node = Node::findOrFail($node_id);
        if(!PermissionsController::checkAccessForNode($node, [1]))//1 -> must have admin permission or editor permission
            return response("<h1 style='color:red'>Permission Denied Access !<h1>", 403);

        $allDoc = Input::get('allDoc');
        $strtingFromNode = 0 ; 
        if(!$allDoc) {
            $strtingFromNode = $node_id;
        }
        DocsPermission::create(['doc_id'=> Input::get('doc_id') , 'start_node_id'=> $strtingFromNode , 'user_id'=> Input::get('user_id'), 'giver_id'=>\Auth::user()->id, 'permission'=> Input::get('permission') ]);

   
          return \Response::json(array(// success response
                'status' => '0' ));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
