<?php

namespace App\Http\Controllers;

use App\Node;
use App\NodesLog;
use Illuminate\Http\Request;

class NodesLogsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function revertToLastChange($newContent, $change)
    {
        $change_arr    = explode("\n", $change);
        $change_count  = count($change_arr); // number of lines to be changed
        $oldLinesCount = $change_arr[0];

        $new_arr   = explode("\n", $newContent);
        $new_count = count($new_arr);
        // dd($change_count);
        for ($i = 1; $i < $change_count; $i++) {
            $atPosition           = strpos($change_arr[$i], '@');
            $lineNumber           = (int) ((int)substr($change_arr[$i], 0, $atPosition) - 1);
            $new_arr[$lineNumber] = substr($change_arr[$i], $atPosition + 1);
        }

        //cut newContent only if $old lines count <  new content lines count
        if ($oldLinesCount < $new_count) {
            array_splice($new_arr, $oldLinesCount);
        }

        return implode("\n", $new_arr);
    }

    public function detectNodeChanges($oldContent, $newContent)//enforces old content

    {
        $old_arr   = explode("\n", $oldContent);
        $old_count = count($old_arr);

        $new_arr   = explode("\n", $newContent);
        $new_count = count($new_arr);
        //dd($new_arr);
        $logs = "";

        if ($new_count >= $old_count) {
            for ($i = 0; $i < $old_count; $i++) {
                if (strcmp($new_arr[$i], $old_arr[$i]) != 0) {
                    $logs .= ($i + 1) . "@{$old_arr[$i]}\n";
                }

            }
            //dd($new_arr);

        } else {
            // handling offset warning when old content is larger using new count
            for ($i = 0; $i < $new_count; $i++) {
                if (strcmp($new_arr[$i], $old_arr[$i]) != 0) {
                    $logs .= ($i + 1) . "@{$old_arr[$i]}\n";
                }
            }
            // get the remaining content after the new content from old content
            for (; $i < $old_count; $i++) {
                $logs .= "\n" . ($i + 1) . "@{$old_arr[$i]}";
            }
        }

        if (!empty($logs)) //if no change, dont add the count
        {
            $logs = $old_count . "\n" . $logs;
        }
        //save old lines count for reverting to past
        return $logs;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function store($nodeId, $summary, $changes)
    {
        $user_id = \Auth::user()->id;
        $now     = date('Y-m-d h:i');
        $log = NodesLog::create(['node_id' => $nodeId, 'user_id' => $user_id, 'summary' => $summary, 'change' => $changes, 'time' => $now]);
        return $log['id'] ;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function updateUsingChanges(Request $request) //F->B, B->B, B->F
    {   //navigation: no log creation
        $now     = date('Y-m-d h:i');
        //$summary   = "empty !";
        $inputs    = $request->all();
        $newState  = $inputs['newState'];
        $changeIds = explode(',', $inputs['changeIds']);
        //get last content
        $node       = Node::where('id', $inputs['nodeId'])->get(['content', 'state','last_commited_log']);
        $oldState   = $node[0]['state'];
        $content    = $node[0]['content'];

        //      down is just a navigation, no logs are added except in the first case
        //check break points in diff paths to neglect in reverting

        //                      F->B
        if ($oldState == 'f' && $newState == 'b') {
            // revert last content(end of path) to selected state in past
            $logs = NodesLog::whereIn('id', array_values($changeIds))->orderBy('id', 'desc')->get(['id', 'change']);
            foreach ($logs as $key => $log) {
                $content = $this->revertToLastChange($content, $log->change);
            }
           
            $lastLogId = $log->id ;
            
            //save last commited log's id in nodes table to use in highlighting current log

        } else if ($oldState == 'b' && $newState == 'b') {
            //                      B->B
            $changes = NodesLog::whereIn('id', array_values($changeIds))->orderBy('id', 'desc')->get(['id', 'change']);
            
            //from current content(in middle of path) to more older one
            foreach ($changes as $key => $log) {
                $content = $this->revertToLastChange($content, $log->change);
            }
            //save last commited log's id in nodes table to use in highlighting current log
            $lastLogId = $log->id ;

        } else if ($oldState == 'b' && $newState == 'f') { // redo from old to saved new
            //                      B->F (new path)
            //from current content(in middle of path) to more newer one and can be in different paths
            $changes = NodesLog::whereIn('id', array_values($changeIds))->orderBy('id', 'asc')->get(['id', 'change']);
            foreach ($changes as $key => $log) {
                $content = $this->revertToLastChange($content, $log->change);
            }

            //save 0 as last commited log's id in nodes table to highlight last one
            $lastLogId = $log->id ;
        }
         

        //update content
        $success = Node::findOrFail($inputs['nodeId'])->update([
                                                    'content' => $content, 
                                                    'state' => $newState,
                                                    'last_commited_log' => $lastLogId
                                    ]);
        session(['notify_revert'=> $success]);
        return redirect()->back();
    }

    //note: comments()->get()->toArray() to convert an eloquent to array

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
