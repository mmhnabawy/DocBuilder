<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\{MeetingIssue, Team};

class MeetingIssuesController extends Controller
{

     public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($team_id)
    {   // get all team issues
        $team_name = Team::where(['id'=>$team_id])->first(['name'])->name;
        // dd( $team_name);
        $issues = MeetingIssue::where(['team_id'=>$team_id])->get();
        $types = [];
        foreach ($issues as $key => $issue) {
             $types[] = $issue->type;
        }
        $types = array_unique($types) ;

        return view('issues', compact('issues', 'types','team_id', 'team_name'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $input = $request->all();
        $input['user_id'] = \Auth::user()->id;
        MeetingIssue::create($input);
        return redirect()->action('MeetingIssuesController@index', ['team_id'=>$input['team_id']]);
    }

      /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function closeIssues(Request $request)
    {   
        $input = $request->all();
        if(isset($input['issues'])) {
            \DB::table('meeting_issues')->whereIn('id', $input['issues'])->update(['open'=> '0' ]);
        }
        return redirect()->action('MeetingIssuesController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
