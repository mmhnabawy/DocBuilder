<?php

namespace App\Http\Controllers;
use App\{NodesReview, Node};
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Response;

class NodesReviewsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($node_id)
    {
        $name = Node::where(['id'=>$node_id])->get(['name'])[0]->name;
        //dd($name);
        $reviews = NodesReview::where(['node_id'=>$node_id])->orderBy('time', 'Asc')->get();
        return view('reviews', compact('reviews','name'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //create review entry

        //check if its our form aginst csrf
        if (\Session::token() !== Input::get('_token')) {
            return Response::json(array(// http response with 403
                'status' => '4','msg' => 'Unauthorized attempt'
            ));
        }
        $id = Input::get('node_id') ;
        $node = Node::findOrFail($id);
        $node->current_version++;
        if(!PermissionsController::checkAccessForNode($node, [1,2,3]))// must have any permission
            return response("<h1 style='color:red'>Permission Denied Access !<h1>", 403);

        $review = ['node_id'=> $id, 'user_id'=>\Auth::user()->id, 'version'=>$node->current_version, 'summary'=> Input::get('summary'), 'time'=>date('Y-m-d H:i:s') ];


        if( NodesReview::create($review)){
            //update node version
            $node->update();
            return Response::json(array(// http response with 403
                'status' => '0','msg' => 'Successfully created a node review !'
            ));
          }


        
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //destroy review entry

        //update node version
    }
}
