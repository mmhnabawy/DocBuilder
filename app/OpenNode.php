<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OpenNode extends Model
{
	protected $fillable = ['node_id','user_id', 'open'];
    public $timestamps = false;
}
