<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NodesLog extends Model
{
   protected $fillable = ['node_id', 'user_id', 'summary', 'change', 'time'];

   public $timestamps = false;

   public function user()
  	{
  		return $this->belongsTo('App\User');
  	}

  	public function node()
  	{
  		return $this->belongsTo('App\Node');
  	}
}
