<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Node extends Model
{	//delete doesn't cascade
  protected $fillable = [ 'name', 'level', 'content','summary', 'parent_id', 'doc_id','current_version', 'last_commited_log', 'state', 'sync_lock'];
  
  public function doc()
  	{
  		return $this->belongsTo('App\Doc');
  	}
}
