<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NodesReview extends Model
{

	protected $fillable = ['node_id', 'user_id', 'version', 'summary', 'time'];
	public $timestamps = false;
  	
  	public function user()
  	{
  		return $this->belongsTo('App\User');
  	}

  	public function node()
  	{
  		return $this->belongsTo('App\Node');
  	}
}
