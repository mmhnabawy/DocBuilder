<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MeetingIssue extends Model
{
     protected $fillable = ['subject', 'type','team_id','user_id', 'open'];

    public function user()
  	{
  		return $this->belongsTo('App\User');
  	}
}
