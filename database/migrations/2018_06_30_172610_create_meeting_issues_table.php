<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMeetingIssuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('meeting_issues', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('subject');
            $table->enum('type', ['Marketing','Design','Logic','Business']);
            $table->biginteger('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->integer('team_id')->unsigned();
            $table->integer('priority')->unsigned()->default(0);
            $table->foreign('team_id')->references('id')->on('teams');
            $table->enum('open', ['0' , '1'])->default('1');
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_unicode_ci';  
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('meeting_issues');
    }
}
