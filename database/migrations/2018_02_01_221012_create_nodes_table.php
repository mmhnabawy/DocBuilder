<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNodesTable extends Migration
{   // last state or log or commit for nodes
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nodes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->tinyInteger('level')->unsigned();
            $table->text('content')->nullable();
            $table->string('summary')->default('empty !');
            $table->bigInteger('parent_id')->unsigned()->nullable();//check performance
            $table->foreign('parent_id')->references('id')->on('nodes')->onDelete('cascade');
            $table->bigInteger('doc_id')->unsigned();
            $table->foreign('doc_id')->references('id')->on('docs')->onDelete('cascade');
            $table->tinyInteger('current_version')->unsigned()->default('0');
            $table->bigInteger('last_commited_log')->unsigned()->nullable();//save last commited log in all case
            //$table->foreign('last_commited_log')->references('id')->on('nodes_logs')->onDelete('cascade');// must add logs table first to add this index be 
            $table->enum('state', array('f', 'b'))->default('f');
            $table->enum('sync_lock', array('locked', 'free'))->default('free');
            $table->timestamps();
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_unicode_ci';  
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('nodes');
    }
}
