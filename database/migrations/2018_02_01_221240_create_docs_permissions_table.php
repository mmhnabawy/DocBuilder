<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocsPermissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()//contributions
    {// need to set the length of permission to int(1)
        Schema::create('docs_permissions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('doc_id')->unsigned();
            $table->foreign('doc_id')->references('id')->on('docs')->onDelete('cascade');;
            $table->bigInteger('start_node_id')->unsigned();
            $table->foreign('start_node_id')->references('id')->on('nodes')->onDelete('cascade');// if 0 then user permission is applied over all the doc
            $table->bigInteger('user_id')->unsigned();//granted ??
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');;
            $table->bigInteger('giver_id')->unsigned();
            $table->foreign('giver_id')->references('id')->on('users');
            $table->tinyInteger('permission')->unsigned();//1-admin, 2-editor, 3-reviewer, 4-reader 
            $table->enum('new', ['0', '1'])->default('1');//used for notification ????
            $table->enum('recursive', ['0', '1'])->default('1');
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_unicode_ci';  
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('docs_permissions');
    }
}
