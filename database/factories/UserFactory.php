<?php

use Faker\Generator as Faker;
use Faker\Provider\DateTime ;
/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'phone'=>$faker->unique()->e164PhoneNumber,
        'gender'=>'m',//str_random(1),
        'password' => '$2y$10$9GT7t9u2w0bWW8PjV4t9QeH.BIKIjavZJXUH2EgP9J1NjxGsKjQuO', // 123456 
        'remember_token' => str_random(10),
    ];
});

//use a given email instead of randoms
$factory->state(App\User::class, 'email', [
    'email' => ['mm_h424@yahoo.com', 'mm_h434@yahoo.com']
]);
// factory(App\User::class, 2)->states('email')->create();
