<?php

use Faker\Generator as Faker;

$factory->define(App\Doc::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'user_id' => 1,
        'created_at' => $faker->dateTime($max = 'now', $timezone = null), // secret
        'updated_at' => $faker->dateTime($min = 'now', $timezone = null),
    ];
});
