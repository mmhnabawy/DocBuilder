<!DOCTYPE html>
<html>
<head>
	
<link rel="stylesheet" type="text/css" href="/Shipping/css/bootstrap.min.css">
<style type="text/css">
#nodes{
width:100%;
height: 100%;
background-color:#EBEDEF;
border-radius:5px;
position: relative;
z-index: 0;
}

.bubble{
position: absolute;
border-radius:50%;
background-color: #F0AD4E;
width: 170px;
height: 170px;
text-align: center;
/*float: left;*/
border: 0px;
cursor:pointer;
display: inline-block;
animation: train_flow;
animation-duration: 0.7s;
animation-fill-mode: initial;
color: white;
}
.bubble:hover{
position: relative;
transition:0.5s all;
top:-10px;
transform:scale(1.2);
}
.container3{
width: 170px;
height: 200px;
display: block;
position: absolute;
z-index: 2;
}
.container3 .bubble a{
text-decoration: none;
color: #4285F4;
}
.container3 .bubble p{
color :white;
text-align: center;
margin: auto;
margin-top:30px;
width:80%;
word-break: break-all;
white-space:normal;
height:50%;
}
.shadow{
position: absolute;
bottom:-25px;
right: 0px;
width: 170px;
height: 30px;
border-radius:50%;
background-color: rgba(2,2,2,0.2);
}
.container2{
position: static;
border:2px solid #641c94;
border-radius: 50%; 
margin-top: 200px;
animation: none;
animation-duration: 1s;
animation-fill-mode: initial;
margin-left: 200px;
z-index: 1;

}

@keyframes train_flow{
0%{
    transform: rotate(360deg);
    right:-700px;
    overflow: hidden;
}
25%{
    transform: rotate(270deg);
    right:-400px;
    overflow: hidden;
}
50%{
    transform: rotate(180deg);
    right:-300px;
    overflow: hidden;
}
75%{
    transform: rotate(90deg);
    right:-200px;
    overflow: hidden;
}

100%{
/*transform: rotate(0deg);*/
right:-85px;
 /*top:-85px;*/
overflow-x:auto;
}
}

.rotate-animation{
animation: rotateTopBubble;
animation-duration: 2s;
/*animation-fill-mode: forwards;*/

}
.control-btns{
    display: none;
}


</style>
</head>
<body>

<div class="control-btns">
    <a href="javascript:;" class="btn btn-info" onclick="rotate(-4)">
        <span class="glyphicon glyphicon-arrow-left"></span>
    </a> 
    &nbsp; 
    <a href="javascript:;" class="btn btn-info" onclick="rotate(4)">
        <span class="glyphicon glyphicon-arrow-right"></span>
    </a>   
</div>


<div id="nodes">
	
<div id="container2" class="container2">
    
<?php for ($i=0; $i < 16 ; $i++) {   ?>
    <div class="container3">
        <div class="bubble" onclick="animateTopBubble(90);">
            Hello <?= $i?>
        </div>
        <div class="shadow"> 
        </div>
    </div>

<?php } ?>

    <a href="" class="btn btn-info" style="position:fixed;bottom:15px;right:15px;height:100px;padding-top:40px;">
        <span class="glyphicon glyphicon-plus"></span>
    </a>
</div>

</div>
<script type="text/javascript" src="/Shipping/js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="/Shipping/js/bootstrap.min.js"></script>

<script type="text/javascript">

var old_deg = 0 ;

var bubbles = document.getElementsByClassName('container3');

function rotate(deg) {

    old_deg += deg;

    for (var i = 0; i < bubbles.length; i++) {
    bubbles[i].style.top = ((( 1 - Math.sin( (i+old_deg) * Math.PI*2/bubbles.length ) )/2) * h) - shift +"px";
    bubbles[i].style.left = ((( 1 + Math.cos( (i+old_deg) * Math.PI*2/bubbles.length ) )/2) * w) + shift +"px";
    }
}


var len = bubbles.length;

var w = ( 70 * len );
var h = ( 70 * len );


$('#container2').css({height:h});
$('#container2').css({ width:w});

var shift = 85 ;

if(len > 7)
{
    //shift the center to right bottom corner of the screen
    var topVal = ($('#container2').height()/2.5) ;
    $('#container2').css({position:'fixed', top:-80, right:-topVal+50 });

    //show rotation buttons
    $('.control-btns').show();

    shift = -10 ;
}



for (var i = 0; i < bubbles.length; i++) {
	bubbles[i].style.top = ((( 1 - Math.sin( i * Math.PI*2/bubbles.length ) )/2) * h) - shift +"px";
	bubbles[i].style.left = ((( 1 + Math.cos( i * Math.PI*2/bubbles.length ) )/2) * w) + shift +"px";
}



</script> 

</body>
</html>
