<?php
use Illuminate\Http\Request;
use App\Http\Controllers\OpenNodesController;
use App\{User};
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/password/change', 'ChangePasswordController@change_form')->name('password.form');
Route::post('/password/change', 'ChangePasswordController@change')->name('password.change');

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::post('/Issues', 'MeetingIssuesController@closeIssues')->name('Issues.closeIssues');
Route::post('/Issues2', 'MeetingIssuesController@store')->name('Issues.store');

Route::get('/Issues/{team_id}', 'MeetingIssuesController@index');


//   chats
Route::resource('/Chat', 'ChatController');
Route::get('/Chat/{team_id}', 'ChatController@index');// get chats of a team
Route::get('/messages/{team_id}', 'ChatController@fetchMessages');
Route::post('/messages', 'ChatController@sendMessage');
Route::get('/chat-teams', function (){	// json by default
	//each team must have more than 1 member to listen for or send to.
	return \DB::select(\DB::raw("SELECT cont1.team_id , teams.name  FROM contributers as cont1 INNER JOIN teams on cont1.team_id=teams.id   WHERE cont1.user_id = 1 and  (select count(cont2.team_id) from contributers as cont2 where cont2.team_id=cont1.team_id  GROUP BY cont2.team_id  )>1"));
	//return Contributer::where('user_id',\Auth::id())->get();
})->middleware('auth');
Route::post('/broadcasting/auth', function ($user) {
  return Auth::check();
})->middleware('auth');
// Docs
Route::resource('/Docs', 'DocsController');
Route::delete('/Docs', 'DocsController@destroyWithAjax');
Route::post('/Docs/export', 'DocsController@export')->name('Docs.export');

// Nodes
Route::resource('/Nodes', 'NodesController');
Route::resource('/Teams', 'TeamsController');

Route::get('/Nodes/create/{doc_id}/{level}/{parent_id}', 'NodesController@create');
Route::get('/Nodes/show/{level}/{doc_node_id}', 'NodesController@show');
Route::post('/Nodes/revert', 'NodesLogsController@updateUsingChanges');
Route::delete('/Nodes', 'NodesController@destroyWithAjax');


Route::get('/Designs', function ()
{
	return view('designs');
})->middleware('auth');

//Route::resource('/Reviews', 'NodesReviewsController');
Route::post('/Reviews', 'NodesReviewsController@store');
Route::get('/Reviews/{node_id}', 'NodesReviewsController@index');

Route::resource('/Contributers', 'ContributersController');
Route::resource('/Permissions', 'PermissionsController');
Route::resource('/GitHooks', 'GitController');

Route::post('/UserCloseNode', function (Request $request)
{	
	//if($request->input('reeferer')); 
	OpenNodesController::updateState($request->input('node_id'), \Auth::user()->id, '0');
	//return Response::json();
});


Route::get('/unlock', function (){
	return view('unlock');
})->middleware('auth');



Route::get('/pasteNode/{nodeId}/{newDocId}/{newParentId}','NodesController@pasteCopied' );

//factory(App\User::class, 1)->create(['email'=>'mm_h434@yahoo.com']);  //make doesn't commit it in db
//factory(App\Role::class, 1)->states('name')->create();



// not found in 5.5 docs ??????????????
// Route::post( '/Contributers/create', array(//url
//     'as' => 'Contributers.create', // route name
//     'uses' => 'ContributersController@create'//controller action
// ) );

//Route::post( '/Contributers/create', 'ContributersController@create' )->name('Contributers.create');

Route::get('/profile', function (){
	$user = \Auth::user();
	return view('profile', compact('user'));
})->middleware('auth');

Route::get('/profile/{user}', function ($id){
	$user = User::findOrFail($id);
	return view('profile', compact('user'));
})->middleware('auth');

// facebook routing
Route::get('/login/facebook', 'Auth\LoginController@redirectToProvider');
Route::get('/login/facebook/callback', 'Auth\LoginController@handleProviderCallback');