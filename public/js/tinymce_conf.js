// WyWIWYS config

var editor_config = {

height : 800,

branding: false,

readonly: 0,

path_absolute: "{{url('/')}}/",

selector: "textarea",

plugins :[ "advlist autolink lists link image charmap print preview hr anchor pagebreak",
           "searchreplace wordcount visualblocks visualchars save fullscreen ",
           "insertdatetime media nonbreaking save table contextmenu directionality",
           "emoticons template paste textcolor colorpicker textpattern"
           ],

toolbar: "insertfile saveas undo redo | cut copy paste | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outindent indent | link image media | forecolor backcolor",

contextmenu: "cut copy paste | link image inserttable | cell row column deletetable",
 paste_data_images: true,
relative_urls : false,

 file_browser_callback : function(field_name, url, type, win) {
      var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
      var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

      var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
      if (type == 'image') {
        cmsURL = cmsURL + "&type=Images";
      } else {
        cmsURL = cmsURL + "&type=Files";
      }

      tinyMCE.activeEditor.windowManager.open({
        file : cmsURL,
        title : 'Filemanager',
        width : x * 0.8,
        height : y * 0.8,
        resizable : "yes",
        close_previous : "no"
      });
    }

} ;

tinymce.init(editor_config);
//console.log(tinymce);


//console.log(tinymce.activeEditor.startContent);
var edit = true;
function toggleReadMode(){
 
     if(edit) {
      tinymce.remove();
      // editor_config.toolbar= false;
      // editor_config.readonly= true;
      // editor_config.menubar =false;
    
    //   console.log(html);
    //   $("#read-div").html(html);
    //   $('#edit-form').hide();
    //   edit = false;

     } else {
       edit = true;
      editor_config.toolbar= true;
      editor_config.readonly= false;
      editor_config.menubar =true;
     }  
  tinymce.init(editor_config);
}