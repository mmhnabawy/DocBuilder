

var yesClickHandler = function (e) {
  doc_id = e.target.param ;
    deleteDoc( doc_id );
    $('#bottomModal').slideToggle();
   } ;
function showModalDelete(doc_id) {

  var bottomModal = document.getElementById('bottomModal');
  var  element =  bottomModal.getElementsByTagName('button')[0] ;//get yes button
  element.param = doc_id;
  //$(element).unbind(); or off();//remove all old listeners on that element
  element.removeEventListener('click', yesClickHandler);// remove old listener to bind a new one
  element.addEventListener('click', yesClickHandler);
  bottomModal.style.display = "block";// using animation css
}

document.addEventListener('DOMContentLoaded', function(){ 
// Get the <span> element that close_bottomModals the bottomModal
var span = document.getElementsByClassName("close_bottomModal")[0];

// When the user clicks on <span> (x), close_bottomModal the bottomModal
span.onclick = function() {
 var bottomModal = document.getElementById('bottomModal');
    bottomModal.style.display = "none";
}
// When the user clicks anywhere outside of the bottomModal, close_bottomModal it
window.onclick = function(event) {
    if (event.target == bottomModal) {
        bottomModal.style.display = "none";
    }
}

});