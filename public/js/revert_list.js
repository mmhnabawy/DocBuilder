document.addEventListener("DOMContentLoaded", function() {

// $(".path.row ul li label").each(function (index, elem) {
//   elem.addEventListener('click', function (e) {
//     //reset
//       $(".path.row ul li label").each(function (index) {
//         $(this).parent().css({'color':'#636b6f'}) ;
//         var font = window.innerWidth > 700 ? 'initial':'1rem';
//         $(this).css({'font-size': font,'text-decoration':'initial','color':'#636b6f','top':'30px'}) ;
//     });

//     //highlight selected
//     $(e.target).css({'background-color':'green','color':'green','top':'40px'});
//     $(e.target).parent().css({'color':'green'});

//       var new_index = $(e.target).parent().index();
//         $(".path.row ul li").each().backwards(function (index) {
//            //console.log(index);
//           if(index >= new_index)
//             return false;
//           //strike newers
//           $(this).children('label').css({'text-decoration':'line-through white'});
//             //$(this).children('label').children('input').prop('checked', true);

//         })

//  })
// });

// revert list

resetChanges = function (e)
{
 $("#changes").children('li').each(function (index, elem) {
  //console.log(elem);
   

    $(this).css({ 'text-decoration':'initial',
                  'color':'#636b6f',
                  'font-weight':'initial'}) ;

     if($(this).hasClass('selected_change'))
        {
          $(this).css({ 'text-decoration':'initial',
                  'color':'white',
                  'font-weight':'initial'}) ;
        }
    //$(this).children('label').children('input').prop('checked', false);
  });
}

var overHandler = function(e) {
  if($(e.target).hasClass('selected_change'))
          return false;

  if (e.target && e.target.matches("li") ) {
      resetChanges();
      var new_index = $(e.target).index();
      var count = $("#changes").children('li').length;
      new_index =  count - new_index - 1 ;
      //console.log(count);
      $(e.target).css({'color':'green', 'font-weight':'bold'});

        $($("#changes").children('li').get().reverse()).each(function (index) {
          //console.log(index);
          if(index >= new_index)
            {
              return false;
            }

           $(this).css({'text-decoration':'line-through white'});
            //$(this).children('label').children('input').prop('checked', true);

        })
      }};


  document.getElementById("changes").addEventListener('mouseover', overHandler);

  document.getElementById("changes").addEventListener('click', function (e) {
      //remove ul and li listeners to stop hover functions
  document.getElementById("changes").removeEventListener('mouseover',overHandler);
  document.getElementById("changes").removeEventListener('mouseleave',resetChanges);

      //if it's the currently selected >> no change
      if($(e.target).hasClass('selected_change'))
          return false;

      //if he clicks again on other choice to change his mind

      overHandler(e);
      //put new change id in form input
      var new_index = $(e.target).index();
      var current_index = $('#changes li.selected_change').index();
      //console.log(current_index);
      var changeIds = "";
      var changes = $("#changes").children('li');
      //set f or b in the input
      //if backing, send ids excluding the recent change, but including oldest one
      if (new_index < current_index) {
          $("#newState").val('b');
          for (var i = current_index-1 ; i >= new_index; i--) {
            changeIds += "," + $(changes[i]).attr('data-changeId');
          }
       } else if(new_index > current_index) {
          $("#newState").val('f');
           for (var i = current_index+1 ; i <= new_index; i++) {
            changeIds += "," + $(changes[i]).attr('data-changeId');
          }

       } else {
          return false;
       }

      //document.querSelector('#changeId').value = "1";;
      $('#changeId').val(changeIds.substr(1));
      //note:can't set  inputs' value with hidden attribute but with type hidden

      //enable button
      $('#revert').attr('disabled', false);

      //if forwarding, send ids including the recent change, excluding the oldest one


    });

  document.getElementById("changes").addEventListener("mouseleave",  resetChanges);

//console.log(lis);
  });



// function createLine(el1, el2){
//     var off1 =getElementProperty(el1);
//     var off2 =getElementProperty(el2);
//     // center of first point
//     var dx1 = off1.left + off1.width/2;
//     var dy1 = off1.top + off1.height/2;
//     // center of second point
//     var dx2 = off2.left + off2.width/2;
//     var dy2 = off2.top + off1.height/2;
//     // distance
//     var length = Math.sqrt(((dx2-dx1) * (dx2-dx1)) + ((dy2-dy1) * (dy2-dy1)));
//     // center
//     var cx = ((dx1 + dx2) / 2) - (length / 2);
//     var cy = ((dy1 + dy2) / 2) - (2  / 2);
//     // angle
//     var angle = Math.atan2((dy1-dy2),(dx1-dx2))*(180/Math.PI);
//     // draw line

//     return  "<section class='connectingLines' style='left:" + cx + "px; top:" + cy + "px; width:" + length + "px; -webkit-transform:rotate(" + angle + "deg); transform:rotate(" + angle + "deg);'></section>";
//   };

// function getElementProperty(el){
//     var dx = 0;
//     var dy = 0;
//     var width = $(el).width()|0;
//     var height = $(el).height()|0;

//     dx += $(el).position().left;//position().left
//     dy += $(el).position().top;

//     return { top: dy, left: dx, width: width, height: height };
// };

//createLine(document.getElementById('Oldest'), document.getElementById('Newest'));
// var canvas = document.getElementById('line');
// var ctx = canvas.getContext("2d");
// ctx.moveTo(10,0);
// ctx.lineTo(10,100);
// ctx.stroke();
// ctx.moveTo(10,100);
// ctx.lineTo(0,90);
// ctx.stroke();
// ctx.moveTo(10,100);
// ctx.lineTo(20,90);
// ctx.stroke();