<!DOCTYPE html>
<html>
<head>
	<title></title>
<style type="text/css">
body{
height:1000px;
}
#nodes{
width:100%;
height: 100%;
background-color:#EBEDEF;
border-radius:5px;
position:relative;
left:0;
z-index: 0;
}
.bubble{
position: absolute;
border-radius:50%;
background-color: #F0AD4E;
width: 170px;
height: 170px;
text-align: center;
/*float: left;*/
border: 0px;
cursor:pointer;
display: inline-block;
animation: train_flow;
animation-duration: 0.7s;
animation-fill-mode: initial;
color: white;
}
.bubble:hover{
position: relative;
transition:0.5s all;
top:-10px;
transform:scale(1.2);
}
.container2{
position: static;
height: 100%;
border:2px solid #641c94;
margin-top: 50px;
margin-left: 50px;
animation: none;
animation-duration: 1s;
animation-fill-mode: initial;
}
.container3{
position: relative;
width: 170px;
height: 200px;
display: block;
float:left;
margin-left:20px;
margin-top:20px;
}
.container3 .bubble a{
text-decoration: none;
color: #4285F4;
}
.container3 .bubble p{
color :white;
text-align: center;
margin: auto;
margin-top:30px;
width:80%;
word-break: break-all;
white-space:normal;
height:50%;
}


.shadow{
position: absolute;
bottom:-10px;
right: 0px;
width: 170px;
height: 30px;
border-radius:50%;
background-color: rgba(2,2,2,0.2);
}
	</style>
</head>
<body>

<div id="nodes">
	
<div id="container2" class="container2">
<?php for ($i=0; $i < 16 ; $i++) {   ?>
    <div class="container3">
        <div class="bubble" onclick="animateTopBubble(90);">
            Hello <?= $i?>
        </div>
        <div class="shadow"> 
        </div>
    </div>

<?php } ?>

    <a href="" class="btn btn-info" style="position:fixed;bottom:15px;right:15px;height:100px;padding-top:40px;">
        <span class="glyphicon glyphicon-plus"></span>
    </a>
</div>

</div>

</body>
</html>