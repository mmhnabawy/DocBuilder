/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
require('./bootstrap');
// window.Vue = require('vue');
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
// Vue.component('chat-messages', require('./components/ChatMessages.vue'));
// Vue.component('chat-form', require('./components/ChatForm.vue'));
// const app = new Vue({
//     el: '#app',
//     data: {
//         messages: []
//     },
//     created() {
//         this.fetchMessages();
//         Echo.private('chat').listen('MessageSent', (e) => {
//             this.messages.push({
//                 message: e.message.message,
//                 user: e.user
//             });
//         });
//     },
//     methods: {
//         fetchMessages() {
//             axios.get('/messages').then(response => {
//                 this.messages = response.data;
//             });
//         },
//         addMessage(message) {
//             this.messages.push(message);
//             axios.post('/messages', message).then(response => {
//                 console.log(response.data);
//             });
//         }
//     }
// });
// console.log(1);

// Trigger for the Enter key when clicked.
$.fn.enterKey = function(fnc) {
    return this.each(function() {
        $(this).keypress(function(ev) {
            var keycode = (ev.keyCode ? ev.keyCode : ev.which);
            if (keycode == '13') {
                fnc.call(this, ev);
            }
        });
    });
}

function showNotification(element_id, elem_class, message){
    $('#'+element_id).removeClass();//clear class list
    $('#'+element_id).addClass(elem_class);
    $('#'+element_id).html(message);
    $('#'+element_id).fadeIn();
}


//check if authenticated first before seding the request
function loadChatTeams(){

    $.ajax({
      method:'GET',
      url:'/DocBuilder/public/chat-teams',
      dataType: 'json'
          
          } ).done(function (data) {
            //console.log(data);
          	var teams = data;
            if(teams.length > 0){
              $("#teams").html('');//clear no teams message
            }
            for (var i = teams.length - 1; i >= 0; i--) {
              //add team to list of teams
              // let func = "toggleChatPanel("+ teams[i].team_id +",'" + teams[i].name +"')";
              let func = "toggleChatPanel("+JSON.stringify( teams[i])+")";

              $("#teams").append("<li onclick='"+func+"'><a href='#'>"+teams[i].name+"</a></li>");
              //listen for that team messages
					     Echo.private('chat-'+ teams[i].team_id).listen('MessageSent', (e) => {
						    //console.log(last_message_user);
                if(selectedTeamMessages.length == 0){//first message ever in the team
                  $('.messages_display').html('');//clear no teams message yet
                }
                if(e.user.id == last_message_user){
                    $('.messages_display').append('<p class = "message_item">'+e.message.message + '</p>');
                }else{
                  $('.messages_display').append('<p class = "message_item">' +e.user.name +" : "+e.message.message + '</p>');
                  last_message_user = e.user.id;
                }
			        $('.input_send_holder').html('<input type = "submit" value = "Send" class = "btn btn-primary btn-block input_send" />');
			        $(".messages_display").scrollTop($(".messages_display")[0].scrollHeight);
					   });
				    }


          }).fail( function(jqXHR, textStatus, errorThrown){
            console.log(jqXHR);
    }) ;
}

loadChatTeams();


