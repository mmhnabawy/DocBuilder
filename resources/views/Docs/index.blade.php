@extends('layouts.app')
<link href="{{ asset('css/docs.css') }}" rel="stylesheet">
<link href="{{ asset('css/bottom_modal.css') }}" rel="stylesheet">

@section('content')
<div class="container" >
    <div class="row">
        <div class="col-md-10 ">
            <div class="panel panel-default">
                <div class="panel-heading">
                  <h2>
                  <img src="/DocBuilder/public/images/papers.png" style="width: 20%; height:auto;max-width: 180px;"> My Documents <span class="badge" style="background-color:#FB6906;font-size:2rem">{{count($Docs)}}</span>
                  <a href="{{url('/Docs/create')}}" class="btn btn-info" style="float:right;font-size: 6rem;">+</a>
                  </h2>
                </div>

                <div class="panel-body">
                 <!--    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif -->
                  <div class="row table-responsive">
                  <table class="table table-bordered table-hover">
                    <thead>
                      <tr>
                        <td>
                         Name 
                        </td>
                        <td>
                         Created at 
                        </td>
                        <td>
                         Modified at 
                        </td>
                        <td>
                         Reviewed/Total
                        </td>
                      </tr>
                    </thead>
                    <tbody>
                       @foreach($Docs as $doc)
                      <tr>
                      <td>
                         <h4 class="col-md-3"><a href="{{url("Nodes/show/1/$doc->id")}}">{{$doc->name}}</a> </h4>
                      </td>
                      <td>
                        {{$doc->created_at}}
                      </td>
                       <td>
                        {{$doc->updated_at}}
                      </td>
                      <td>
                        25/30
                      </td>
                       <td>
                         <!-- {!! Form::open([ 'method'=>'DELETE', 'route'=>['Docs.destroy', $doc->id ], 'class'=>'col-md-1' ]) !!} -->
                     <button  type="button" onclick="showModalDelete({{$doc->id}})" type="button" class='btn btn-danger' style='font-size:2rem;width:50px;height:40px; text-align:center;' title="delete whole document"><span class="glyphicon glyphicon-trash"></span></button>
                      <!-- {!! Form::close() !!} -->
                      </td>
                      <td>
                      {!! Form::open([ 'method'=>'POST', 'route'=>['Docs.export', $doc->id ],'class'=>'col-md-1' ]) !!}
                     <button  type="submit" class ='btn btn-danger ' style='font-weight:bold;width:50px;'> <img style="width:30px;" src="{{asset('images/export-3.png')}}" title="export whole document"></button>
                      {!! Form::close() !!}
                      
                      </td>
                    </tr>
                     @endforeach
                  </tbody>
                  </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-2 chat-panel" >
          <h3 onclick="shrinkPanel()" class="chat-head"><span class="glyphicon glyphicon-comment"></span> Chat Panel</h3>
          <div class="chat-mate">
            <div class="chat-active"></div><p>mohamed nabawy mohamed</p>
          </div>

        </div>
    </div>
</div>



  <!--          The bottomModal                 -->
<div id="bottomModal" class="bottomModal">
  <!-- bottomModal content -->
  <div class="bottomModal-content">
    <div class="bottomModal-header">
      <span class="close_bottomModal">&times;</span>
      <h2>Are you sure to remove this Document ?</h2>
    </div>
    <div class="bottomModal-body">
      <button  class="btn btn-danger">Yes</button>
      &nbsp;&nbsp;&nbsp;
      <button class="btn btn-link" onclick="$('#bottomModal').slideToggle();">No</button>
    </div>
    <div class="bottomModal-footer">
      <h3></h3>
    </div>
  </div>
</div>
@endsection

<script type="text/javascript" src="{{ URL::asset('js/bottom_modal_docs.js') }}"></script>

<script type="text/javascript">

document.addEventListener("DOMContentLoaded", function() {

$('.chat-panel').click(function(event){//show when width < 900
  if($('.chat-panel').height() <= 50 && window.innerWidth < 990) {
  $(this).css({height:'500px',position:'absolute',top:50, right:5});
  }
});

});

function shrinkPanel(event)//hide when width < 990
{
  if($('.chat-panel').height() > 50 && window.innerWidth < 990) {
     // console.log(document.querySelector('div.chat-panel'));
      //document.querySelector('div.chat-panel').style.position= 'fixed';
      setTimeout(function(){
        $('.chat-panel').css({position:'relative',height:'50px',width:'150px', overflow:'hidden',cursor:'pointer'});      
      }, 500);
     
}
}

function deleteDoc(DocId)
{
    var data = {
                 // "_token": $( this ).find( 'input[name=_token]' ).val(),
                  "doc_id": DocId,
                  "_token": $('meta[name="csrf-token"]').attr('content')
              };


    $.ajax({
        headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
      method:'DELETE',
      url:"{{url('/Docs')}}",
      data: JSON.stringify(data),
      contentType:'application/json; charset=utf-8',
      dataType: 'json' 
          
          } ).done(function (response) {
           console.log(response);
            if (response.status == 0) {                 
                location.reload();
            }
          }).fail( function(jqXHR, textStatus, errorThrown){
              console.log(jqXHR);
              }) ;
   
  }
</script>