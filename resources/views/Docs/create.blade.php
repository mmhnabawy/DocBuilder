@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Add New Document</div>

                <div class="panel-body">
                {!! Form::open(array('route'=>'Docs.store')) !!}
                    <div class="form-group">
                    {!! Form::text('name', null, array('required','autofocus', 'class'=>'form-control', 'placeholder'=>'Document Name ', 'autocomplete'=>'off') ) !!}
                    </div>
                    <div class="form-group">
                    {!! Form::select('template',[0 => 'software'] ,null, array( 'class'=>'form-control', 'placeholder'=>'Choose a template ') ) !!}
                    </div>
                      <div class="form-group">
                    {!! Form::submit('add', array('required', 'class'=>'btn btn-primary') ) !!}
                    </div>
                {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
