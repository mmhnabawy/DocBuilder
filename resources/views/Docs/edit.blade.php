@extends('layouts.app')


@section('content')
<?php session(['currentDocId' => $doc->id]); ?>
<link href="{{ URL::asset('css/nodes.css') }}" rel="stylesheet">
<div class="container">
   <div class="row"><a href="{{url('/Docs')}}"> <img style="background-color:#3097D1;" src="{{URL::asset('images/pointer_left.png')}}"></a>
   </div> 
    <div class="row">
        <div class="col-md-5 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Edit Document</div>

                <div class="panel-body">
                {!! Form::open(array( 'method'=>'PATCH' , 'route'=>['Docs.update', $doc->id])) !!}
                    <div class="form-group">
                    {!! Form::text('name', $doc->name, array('required', 'class'=>'form-control', 'placeholder'=>'Document Name ') ) !!}
                    </div>
                      <div class="form-group">
                    {!! Form::submit('update name', array( 'class'=>'btn btn-primary') ) !!}
                    </div>
                {!! Form::close() !!}

                </div>
            </div>
        </div>


    </div>
</div>


@endsection

