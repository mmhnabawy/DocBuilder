<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/chat.css') }}" rel="stylesheet">

    <!-- app.js contains jquery -->
    <link href="https://fonts.googleapis.com/css?family=Roboto+Slab" rel="stylesheet">

    <style type="text/css">
        .active{
            background-color:white;
            color:#4B76D4;
        }
        body{
            font-family: 'Roboto Slab', serif;
        }
        .btn-info{
            background-color: #198bcf;
        }
        .btn-danger{
          background-color: #bc0f43;
        }
        .navbar-default .navbar-nav > li > a{
            color: rgb(13, 239, 103);
        }
        .navbar-default .navbar-nav > li > a:hover, .navbar-default .navbar-nav > li > a:focus{
            color:#ffffff;
        }
        .nav .open > a, .nav .open > a:hover, .nav .open > a:focus{
            background-color:unset;
        }
        .navbar-default .navbar-nav > .open > a, .navbar-default .navbar-nav > .open > a:hover, .navbar-default .navbar-nav > .open > a:focus{
            background-color:unset;
        }
        .navbar-default .navbar-nav > .active > a, .navbar-default .navbar-nav > .active > a:hover, .navbar-default .navbar-nav > .active > a:focus{
            font-weight: bold;
            color: #ffffff;
            background-color:#7548ee;
        }
    </style>
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top" >
            <div class="container" style="background: linear-gradient(-135deg, rgb(134, 73, 214) 0px, rgb(39, 145, 210) 100%);">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse" aria-expanded="false">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a  style="color:rgb(239, 173, 78);" class="navbar-brand" href="{{ url('/') }}">
                        {{ config('app.name', 'Laravel') }}
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    @if(Auth::check())
                    <ul class="nav navbar-nav" >
                        &nbsp;
                        <li class="{{url()->current()==url('/Docs')?'active':''}}">
                            <a  href="{{url('/Docs')}}"> Docs</a>
                        </li>
                        <li class="{{url()->current()==url('/Designs')?'active':''}}">
                            <a href="{{url('/Designs')}}"> Designs</a>
                        </li>

                        <li class="{{url()->current()==url('/Contributers')?'active':''}}">
                            <a  style="display:inline-block;" href="{{url('/Contributers')}}">Collaborators </a>

                        @if(session()->has('contributions') && session('contributions')>0)
                        <span class="badge" style="background-color:#FB6906;">{{session('contributions')}}</span>
                        <?php session()->pull('contributions');?>
                        @endif
                        </li>
                        
                         <li class="{{url()->current()==url('/GitHooks')?'active':''}}">
                            <a href="{{url('/GitHooks')}}">Git hooks</a>
                        </li>
                       
                    </ul>

                        <div class="dropdown chat-icon">
                            <button class="btn btn-info dropdown-toggle chat-btn"  type="button" data-toggle="dropdown" aria-expanded="false">
                                <img style="width:50px;display:block" src="{{asset('images/user-group-chat.png')}}"> team chat</button>
                          <ul id="teams" class="dropdown-menu">
                            <li>Empty List !</li>
                          </ul>
                        </div>

                    @endif
                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right" >
                        <!-- Authentication Links -->
                        @guest
                            <li><a style="color: #ffd401;" href="{{ route('login') }}">Login</a></li>
                            <li><a style="color: #ffd401;" href="{{ route('register') }}">Register</a></li>
                        @else
                            <li class="dropdown" >
                                <a style="color: #ffd401;" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu">
                                    <li>
                                       <a href="{{url('profile')}}">My Profile</a> 
                                    </li>
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        @yield('content')

         @if(Auth::check())
            <div class = "chat-panel row">       
                <div class = "col-md-12 chat_box" id="chatbox">    
                    <input type="hidden" id="team_id" name="" value="0">
                    <div id="team_chat_head" onclick="if(event.target.matches('#team_chat_head')){$('#chat-body').slideToggle();}" class="text-center well">No team selected
                    </div>
                    <div id="chat-body">
                        <div class="form-control messages_display"></div>
                        <br/>                                      
                        <div class = "form-group">                      
                            <textarea id="message" class="input_message form-control" placeholder = "Enter Message" rows="3"></textarea>
                        </div>                      
                        <div class = "form-group input_send_holder">            
                            <input type = "submit" value = "Send" class = "btn btn-primary btn-block input_send" />         
                        </div>   
                    </div>
                </div>               
            </div>
        @endif

    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js')}}"></script>

<script type="text/javascript" >

var selectedTeamMessages, last_message_user ;

function loadTeamMessages(team_id){

$.ajax({
  method:'GET',
  url:'/DocBuilder/public/messages/'+team_id,
  dataType: 'json'
      
      } ).done(function (data) {
        // console.log(Array.isArray(data));
            selectedTeamMessages = data;
            if(selectedTeamMessages.length == 0){
                $('.messages_display').append('<p class = "text-warning">There is no message yet !</p>');
            }
            for (var i = selectedTeamMessages.length - 1; i >= 0; i--) {
                if(selectedTeamMessages[i].user_id == last_message_user){
                    $('.messages_display').append('<p class = "message_item">'+selectedTeamMessages[i].message + '</p>');
                }else{
                    $('.messages_display').append('<p class = "message_item">' +selectedTeamMessages[i].name +" : "+selectedTeamMessages[i].message + '</p>');
                    last_message_user = selectedTeamMessages[i].user_id;
                }
              
            
              $(".messages_display").scrollTop($(".messages_display")[0].scrollHeight);
            }


      }).fail( function(jqXHR, textStatus, errorThrown){
        // console.log(jqXHR);
         $('.messages_display').append('<p class= "text-danger"> Error Occured while loading messages !</p>');
    }) ;
}

var current_team_id = 0;//closed window
function toggleChatPanel(team){//toggle or switch
    $('.messages_display').html('');//clear messages

   if( current_team_id == 0 ){
        $('.chat-panel').slideDown();//open

    }else if(current_team_id == team.team_id){
        $('.chat-panel').slideUp();//close if it's the same team
        current_team_id = 0;
        return
    }

    $('#team_chat_head').html(team.name+"<a href='#' class='label label-primary pull-right' style='font-size:1.5rem;' onclick='toggleChatPanel("+JSON.stringify(team)+")'>x</a>");
    $('#team_id').val(team.team_id);
    //set flag to last selected team id
    current_team_id = team.team_id;
    loadTeamMessages(current_team_id);
}


function sendMessage(){
    var data = {
             // "_token": $( this ).find( 'input[name=_token]' ).val(),
              "message": $( '#message' ).val(),
              "team_id": $( '#team_id' ).val(),
              "_token": $('meta[name="csrf-token"]').attr('content')
             
          };

      $.ajaxSetup({
              headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              }
      });


    $.ajax({
      method:'POST',
      url:"{{url('/messages')}}",
      data: JSON.stringify(data),
      contentType:'application/json; charset=utf-8',
      dataType: 'json' 
          
          } ).done(function (data) {
            console.log(data);
            if (data.status == 0) {
                showNotification('#team-members-alert', 'alert-success', 'user added to list successfully !');
            }else if (data.status == 1) {
                showNotification('#team-members-alert', 'alert-warning', 'user already exist in the list !');
            }else if (data.status == 2) {
                showNotification('#team-members-alert', 'alert-danger', 'user not registered on the app !');
            }else if (data.status == 4) {
                showNotification('#team-members-alert', 'alert-danger', 'can not register this on the list !');
            }

          }).fail( function(jqXHR, textStatus, errorThrown){
            console.log(jqXHR);
    }) ;
}

// Send the Message enter by User
$('body').on('click', '.chat_box .input_send', function(e) {
    e.preventDefault();
    var message = $('.chat_box .input_message').val();

    if (message !== '') {
        // Define ajax data
        var chat_message = {
            name: $('.chat_box .input_name').val(),
            message: '<strong>' + $('.chat_box .input_name').val() + '</strong>: ' + message
        }
        //console.log(chat_message);
        // Send the message to the server passing File Url and chat person name & message
        sendMessage();
        // Clear the message input field
        $('.chat_box .input_message').val('');
        // Show a loading image while sending
        $('.input_send_holder').html('<input type = "submit" value = "Send" class = "btn btn-primary btn-block" disabled /> &nbsp;<img src="images/gifs/loading.gif" style="width:50px;height:50px;" />');
    }
});
// Send the message when enter key is clicked
$('.chat_box .input_message').enterKey(function(e) {
    e.preventDefault();
    $('.chat_box .input_send').click();
}); 



// var current_user = {{\Auth::id()}};
</script>

</body>
</html>
