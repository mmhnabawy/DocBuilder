@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading"><h3> Unlock Levels and nodes and go infinity <img style="width:100px;" src="{{asset('images/infinity.png')}}"></h3></div>

                <div class="panel-body">
                   
                   {!! Form::open([ 'route' =>'Nodes.store' ]) !!}
                   
                   <!-- hint : doc_id field  not used in controller -->
        
                    <div class="form-group">
                   {!! Form::text('name', null, ['required', 'placeholder'=>'node name', 'class'=>'form-control']) !!}
                   </div>
                   
                    <div class="form-group">
                   {!! Form::textarea('content', null, [ 'placeholder'=>'node content', 'class'=>'form-control']) !!}
                   </div>
                   <!--i level 1 , parent_id = doc_id-->
                    <div class="form-group">
                   {!! Form::submit('add', [ 'class'=>'btn btn-info']) !!}
                    </div>
                   {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
</div>



@endsection
