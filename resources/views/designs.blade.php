@extends('layouts.app')
<style type="text/css">
.container .row .heading{
background-color:orange;
padding:10px;
border-radius:5px;
color:white;
text-align:center;
}
#search-btn{
	background-position:right;
	background-repeat:no-repeat;
	background-image:url({{asset('images/search.png')}});
	
}
.templates a{
	margin-top:10px;
}
	
</style>
@section('content')

<div class="container">
<div class="row">

<div class="templates">	
	<h2 class="heading" >Templates </h2>
	<a class="btn btn-default" href="" >
		<span style="display:block;color:orange;width:100px;height:100px;font-size:10rem;" class="glyphicon glyphicon-plus"></span>
		Blank
	</a>

	<a class="btn btn-default" href="" >
		<img src="{{asset('images/erd.png')}}" style="display:block;width:100px;height:100px;">
		Erd
	</a>

	<a class="btn btn-default" href="" >
		<img src="{{asset('images/mindmap.png')}}" style="display:block;width:100px;height:100px;">
		Mind Map
	</a>

	<a class="btn btn-default" href="" >
		<img src="{{asset('images/flowchart.png')}}" style="display:block;width:100px;height:100px;">
		Flowchart
	</a>

	<a class="btn btn-default" href="" >
		<img src="{{asset('images/education.png')}}" style="display:block;width:100px;height:100px;">
		Education
	</a>



	<hr style="background-color:black;">
</div>


<div>
	
	<input id="search-btn" class="form-control" type="text" placeholder="search designs" name="search"/>
	<a href=""><img src="{{asset('images/sort1.png')}}"></a>
	<a href=""><img src="{{asset('images/sort2.png')}}"></a>
</div>

<div>
	<h2 class="heading">My designs</h2>
	
	<a class="btn btn-default" href="" >
		<img src="{{asset('images/education.png')}}" style="display:block;width:100px;height:100px;" alt="thumbnail of your design">
		<label>Custom Name</label> 
	</a>
</div>
	

</div>

</div>

@endsection