@extends('layouts.app')

@section('content')
<link href="{{URL::asset('css/home.css')}}" rel="stylesheet" type="text/css">
<link href="{{URL::asset('css/issues.css')}}" rel="stylesheet" type="text/css">

    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="text-center">{{$team_name}} Meeting Issues <a href="#new-issue" class="btn btn-info btn-lg pull-right" onclick=" $('html, body').animate({scrollTop: $('#new-issue').offset().top-50}, 'slow');"><i class="glyphicon glyphicon-arrow-down"></i></a> </h3>
                    </div>
                    <div class="panel panel-default">

                    <div class="panel-body text-center">
                      @if(count($issues)==0)
                        <h5 class="text-center text-warning">No issues placed Yet !</h5>
                      @else
                        {!! Form::open(array('method'=>'post', 'route'=>'Issues.closeIssues')) !!}
                          @foreach($types as $type)
                            <table class="table table-bordered table-hover">
                                <h3 class="text-center issue-header">{{$type}} issues</h3>
                                <hr>
                                <thead>
                                  <tr>
                                    <td>
                                     Subject 
                                    </td>
                                    <td>
                                    Created at 
                                    </td>
                                    <td>
                                    By
                                    </td>
                                  </tr>
                                </thead>
                                <tbody>
                                @foreach($issues as $issue)
                                    @if( $issue->type == $type)
                                      <tr>
                                      <td>
                                        <label>
                                        <input type="checkbox" name="issues[]" value="{{$issue->id}}" {{$issue->open? '':'checked'}}>
                                        &nbsp;{{$issue->subject}}
                                        </label>
                                      </td>
                                      <td>
                                        {{$issue->created_at}}
                                      </td>
                                       <td style="font-size:1.3rem;">
                                        {{$issue->user->name}}                 
                                      </td>
                                    </tr>
                                    @endif
                                @endforeach
                                </tbody>
                              </table>
                          @endforeach
                          <input class="btn btn-warning btn-lg" type="submit" name="submit" value="Close Issues">
                            
                        {!! Form::close() !!}
                      @endif
                    </div>
                        <hr>
                        <div id="new-issue" class="panel-body text-center">
                            {!! Form::open(array('route'=>'Issues.store')) !!}
                             <input type="hidden" name="team_id" value="{{$team_id}}">
                             
                             <div class="form-group">
                             <h3>Create new issue</h3>
                             </div>
                            
                            <div class="form-group">
                             <textarea class="form-control" name="subject" placeholder="leave a small description here !" maxlength="255" required="">
                             </textarea>
                             </div>
                             <div class="form-group">
                                <label>About</label>
                                 <select class="form-control" name="type" required="">
                                    <option value="Marketing">Marketing</option>
                                    <option value="Design">Design</option>
                                    <option value="Logic">Logic</option>
                                    <option value="Business">Business</option>
                                 </select>
                             </div>
                            <input class="btn btn-info btn-lg" type="submit" name="submit" value="+ Issue">
                           {!! Form::close() !!}
                        </div>

                        
                    </div>
                
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" >
        
        
    </script>
@endsection
