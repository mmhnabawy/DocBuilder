@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Add New Team</div>

                <div class="panel-body text-center">
                {!! Form::open(array('route'=>'Teams.store')) !!}
                    <div class="form-group">
                    {!! Form::text('name', null, array('required','autofocus', 'class'=>'form-control text-center', 'placeholder'=>'Team Name ', 'autocomplete'=>'off') ) !!}
                    </div>
                    <div class="form-group">
                    {!! Form::submit('add', array('required', 'class'=>'btn btn-primary') ) !!}
                    </div>
                {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
