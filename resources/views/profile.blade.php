@extends('layouts.app')
<style type="text/css">
.container .row .heading{
background-color:#64B5F6;
padding:10px;
border-radius:5px;
color:white;
}
	
</style>
@section('content')

<div class="container">
<div class="row">

<div>	
	<h2 class="heading">Info </h2>
	<!-- personal info -->
	<img class="img-thumbnail" style="width:300px;height:300px;" src="{{url($user->photo)}}"  alt="Couldn't load the image">
	<h3>Name : {{$user->name}}</h3>
	<h3>Email : {{$user->email}}</h3>
	<h3>Phone : {{$user->phone}}</h3>
	<a href="{{url('/password/change')}}" style="float:right">Change password</a>
	<h4>Other Presence :</h4>
	<hr style="background-color:black;">
</div>


<!-- docs contributions (own, admin, reviewed ) -->
<div>
	<h2 class="heading">Contributions</h2>
	<table class="table table-striped">
		<thead>
			<tr>
				<th>
				Doc
				</th>
				<th>
				Role
				</th>	
			</tr>
			
		</thead>
	
		<tbody>
			<tr>
				<td>
					blabla
				</td>
				<td>
					admin
				</td>
			</tr>
		</tbody>	
	</table>
</div>
	<!-- settings & preferences -->

</div>

</div>

@endsection