@extends('layouts.app')
<link rel="stylesheet" type="text/css" href="{{URL::asset('css/contributers.css')}}">
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-0">
            <div class="panel panel-default">
                <div class="panel-heading">
                  Contributers
                  <!-- <input type="text" name="_token" value="{{csrf_token()}}" hidden=""> -->
                </div>
                <div class="panel-body team">
                    <img style="width:100px;" src="{{asset('images/network.png')}}">
                    <h3 class="text-info" style="display:inline;">Teams 
                      <div class="pull-right" style="display:flex;">
                      
                      <a href="{{url('Teams/create')}}" class="btn btn-info btn-lg" style="font-size:2rem" title="add new team"><i class="glyphicon glyphicon-plus"></i></a>
                
                      </div>
                    </h3> 

                  <h4 id="teams-alert" class="alert">Notification</h4>

                 <div class="row table-responsive">
                  <table class="table table-bordered table-hover">
                    @if(count($teams) == 0)
                       <thead>
                          <h3 class="text-center">
                              Empty List !
                          </h3>
                        </thead>
                    @else
                    <thead>
                      <tr>
                        <td>
                         <h3>Name</h3>
                        </td>
                         <td>
                          <h3>Members #</h3>
                        </td>
                        <td>
                          <h3>Issues #</h3>
                        </td>
                      </tr>
                    </thead>
                    <tbody>
                       @foreach ($teams as $team)
                        <tr class="contributer">
                        <td>
                          <h4>
                            {{$team->name}}
                          </h4>
                        </td>
                        <td>
                          <h4>
                            {{$team->members}}
                          </h4>
                        </td>
                        <td>
                          <h4>
                          {{$team->issues}}&nbsp;&nbsp;
                          <a href="{{url('/Issues/'.$team->id)}}">Edit</a>
                          </h4>
                        </td>
                      </tr>
                    @endforeach 
                  @endif 
                  </tbody>
                  </table>
                </div>
              
            </div>
                <div class="panel-body team">
                    <img style="width:100px;" src="{{asset('images/users_2.png')}}">
                    <h3 class="text-info" style="display:inline;">Team Memebers 
                      <div class="pull-right" style="display:flex;">
                      <input id="email"  name="email" class="form-control" type="email" placeholder="add user to team" style="margin-left:10px;" />
                       <select id="team"  name="team" class="form-control" type="text"  style="margin-left:10px;">
                        @foreach ($teams as $team)
                         <option value="{{$team->id}}">{{$team->name}}</option>
                         @endforeach 
                       </select>
                      <button title="add user to your team list." onclick="add_contributer();" class="btn btn-info" style="margin-left:10px;font-size:2rem;"><i class="glyphicon glyphicon-plus"></i></button>
                
                    </div>
                  </h3> 

                  <h4 id="team-members-alert" class="alert">Notification</h4>

                 <div class="row table-responsive">
                  <table class="table table-bordered table-hover">
                      @if(count($contributers) == 0)
                       <thead>
                          <h3 class="text-center">
                              Empty List !
                          </h3>
                        </thead>
                    @else
                    <thead>
                      <tr>
                        <td>
                         <h3>Member</h3>
                        </td>
                        <td>
                           <h3>Email</h3> 
                        </td>
                         <td>
                           <h3>Team</h3> 
                        </td>
                      </tr>
                    </thead>
                    <tbody>
                       @foreach ($contributers as $contributer)   
                          <tr class="contributer">
                          <td>
                            <h4>
                              {{$contributer->name}}
                            </h4>
                          </td>
                          <td>
                          <h4>
                            {{$contributer->email}}
                            <button class="btn btn-danger" style="float:right;display:none;">x</button>
                          </h4>
                          </td>
                          <td>
                            <h4>
                              {{$contributer->team}}
                            </h4>
                          </td>
                        </tr>
                    @endforeach 
                  @endif 
                  </tbody>
                  </table>
                </div>
              
            </div>


            <div class="panel panel-default">
                <div class="panel-heading"> <img style="width:100px; " src="{{asset('images/assign_tasks.ico')}}"> <h3 class="text-info" style="display:inline;"> Tasks Assignments</h3></div>

                <div class="panel-body assigned-contributions table-responsive">
                      <table class="table table-bordered table-hover">
                         @if(count($assigns) == 0)
                         <thead>
                            <h3 class="text-center">
                              Empty List !
                            </h3>
                          </thead>
                        @else
                        <thead>
                          <tr>
                            <td>
                             <h3>Member</h3>
                            </td>
                            <td>
                               <h3>Node</h3> 
                            </td>
                            <td>
                               <h3>Role</h3> 
                            </td>
                            <td>
                               <h3>Deadline</h3> 
                            </td>
                          </tr>
                        </thead>
                        <tbody>
                           @foreach($assigns as $assign) 
                            <tr class="contributer">
                            <td>
                              <h4>
                                <h4>
                              <a href="{{url('/profile/'.$assign->user_id)}}"> {{$assign->user_name}}</a>
                            </h4>
                              </h4>
                            </td>
                            <td>
                             <h4>{{$assign->node_name}}</h4>
                            </td>
                             <td>
                             <h4>{{$assign->permission}}</h4>
                            </td>
                             <td>
                             <h4>-----</h4>
                            </td>
                          </tr>
                        @endforeach 
                      @endif 
                      </tbody>
                    </table>
                </div>
            </div>

            <div class="panel panel-default">
                    <div class="panel-heading">
                       <img style="width:100px;" src="{{asset('images/task.jpg')}}"> <h3 class="text-info" style="display:inline;"> My Contributions</h3>
                    </div>

                <div class="panel-body my-contributions table-responsive">
                  <table class="table table-bordered table-hover">
                       @if(count($myContributions) == 0)
                         <thead>
                            <h3 class="text-center">
                              Empty List !
                            </h3>
                          </thead>
                        @else
                        <thead>
                          <tr>
                            <td>
                             <h3>Doc</h3>
                            </td>
                            <td>
                               <h3>Node</h3> 
                            </td>
                            <td>
                               <h3>Role</h3> 
                            </td>
                            <td>
                               <h3>Deadline</h3> 
                            </td>
                          </tr>
                        </thead>
                        <tbody>
                          @foreach($myContributions as  $contribution)
                            <tr class="contributer">
                            <td>
                              <h4>
                                {{$contribution->doc_name}}
                              </h4>
                            </td>
                            <td>
                              <h4>
                                <a href="{{url("Nodes/show/$contribution->nextLevel/$contribution->node_id")}}">{{$contribution->node_name}}</a>
                              </h4>
                            </td>
                             <td>
                              <h4>{{$contribution->permission}}</h4>
                            </td>
                             <td>
                             <h4>-----</h4>
                            </td>
                          </tr>
                        @endforeach 
                      @endif 
                      </tbody>
                    </table>
                    </div>    
                </div>
            </div>
        </div>
    </div>
</div>



<script type="text/javascript">
 // $(document).ready( function() {
 
   // $( '#form-submit' ).on( 'submit', function() {
 
        //.....
        //show some spinner etc to indicate operation in progress
        //.....
 
 function add_contributer()
 {
    var data = {
                 // "_token": $( this ).find( 'input[name=_token]' ).val(),
                  "email": $( '#email' ).val(),
                  "team": $( '#team' ).val(),
                  "_token": $('meta[name="csrf-token"]').attr('content')
                 
              };

      $.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  }
      });


    $.ajax({
      method:'POST',
      url:"{{url('/Contributers')}}",
      data: JSON.stringify(data),
      contentType:'application/json; charset=utf-8',
      dataType: 'json' 
          
          } ).done(function (data) {
            if (data.status == 0) {

                $('#team-members-alert').removeClass();//clear class list
                $('#team-members-alert').addClass('alert-success');
                $('#team-members-alert').html('user added to list successfully !');
                $('#team-members-alert').fadeIn();
            }
            else if (data.status == 1) {
              console.log(data.status);
                $('#team-members-alert').removeClass();//clear class list
                $('#team-members-alert').addClass('alert-warning');
                $('#team-members-alert').html('user already exist in the list !');
                $('#team-members-alert').fadeIn();
            }
            else if (data.status == 2) {
                $('#team-members-alert').removeClass();//clear class list
                $('#team-members-alert').addClass('alert-danger');
                $('#team-members-alert').html('user not registered on the app !');
                $('#team-members-alert').fadeIn();
            }
            else if (data.status == 4) {
                $('#team-members-alert').removeClass();//clear class list
                $('#team-members-alert').addClass('alert-danger');
                $('#team-members-alert').html('can not register this on the list !');
                $('#team-members-alert').fadeIn();
            }

          }).fail( function(jqXHR, textStatus, errorThrown){
            console.log(jqXHR);
    }) ;
   
  }

        // $.post( // $( this ).prop( 'action' )
        //    "{{route('Contributers.create')}}",
        //    JSON.stringify(data),
        //     function( data ) {
        //         //do something with data/response returned by server
        //         console.log(data);
        //     },
        //     'json'
        // );
        //.....
        //do anything else you might want to do
        //.....
 
        //prevent the form from actually submitting in browser
        //return false;
        
   // } );
 
//} );
  
</script>

@endsection
 <!--   <input list="docs"  name=""  style="text-align:center;">  
                  <datalist id="docs" > 
                    <option value="">
                    </datalist> -->