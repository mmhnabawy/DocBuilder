<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                /*color: #636b6f;*/
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 20px;
                
                font-family:'Raleway';
            }

            .links > a {
                color: rgb(13, 239, 103);
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body style="color:white;background-image:url(images/docxhub.jpg);background-repeat:no-repeat;background-size:cover;" >
        <div style="background: linear-gradient(-135deg, rgba(134, 73, 214, 0.6) 0px, rgba(39, 145, 210, 0.6) 100%);">
            <div class="flex-center position-ref full-height" >
                @if (Route::has('login'))
                    <div class="top-right links" style="color:#ffffff;">
                        @auth
                            <a href="{{ url('/home') }}">Home</a>
                        @else
                            <a href="{{ route('login') }}">Login</a>
                            <a href="{{ route('register') }}">Register</a>
                        @endauth
                    </div>
                @endif

                <div class="content">
                  <div class="logo" style="color:rgb(239, 173, 78);position:relative;width:300px;height:200px;">
                    <h1 style="transform:rotate(10deg);">DocBuilder</h1>
                    <p style="display: inline-block;transform:rotate(90deg);width:70px;margin-bottom:0px;font-size:32px;font-weight:bold;position:absolute;top: 5px;right: 20px;">....</p>
                    <img src="http://127.0.0.1/DocBuilder/public/images/document.png" style="width:50px;height:50px;position:absolute;right: 20px;bottom:0px;top: 70px;">
                    </div>
                     <div class="title m-b-md">
                        Get your docs done as if you have coffee <?= "\u{2615}" ?> 
                    </div>

                    <div class="title m-b-md">
                        You don't have to be an expert to create a documentation !
                    </div>
                    <div class="title m-b-md">
                        we've setup everything for you to go immediately and do it easily !
                    </div>

                     <div class="title m-b-md">
                        Maintain your software without extra cost and it's engaged with git to document commit-based changes one-by-one.
                    </div>


                    <div class="links">
                        <a href="{{url('/Docs')}}">Create Doc</a>
                        <a href="https://laracasts.com">Manage your team</a>
                        <a href="https://laravel-news.com">News</a>
                        <a href="{{url('/feedback')}}">Feedback</a>
                        <a href="{{url('/contact')}}">Contact us</a>
                        <a href="{{url('/help')}}">Help</a>

                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
