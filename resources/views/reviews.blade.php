@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
            <div class="panel panel-default">
                <div class="panel-heading"><h3><img style="width:70px;display:inline" src="{{asset('images/review_2.png')}}" onclick="">Reviews for {{$name}} </h3></div>

                <div class="table-responsive panel-body">
                	 <table class="table table-bordered table-hover">
                    <thead>
                      <tr>
                      	<td>
                        Version 
                        </td>
                        <td>
                        Summary 
                        </td>
                        <td>
                        Signed By
                        </td>
                        <td>
                        at
                        </td>
                      </tr>
                    </thead>
                    <tbody>
                    @foreach($reviews as $review)
                      <tr>
                      	<td>
                        	{{$review->version}}
                      	</td>
                      	<td>
                        	{{$review->summary}}
	                    </td>
	                    <td>
                        	<h4>
                        		{{$review->user->name}}
                        	</h4>
                      	</td>
                     
                       	<td>
                        	{{$review->time}}
                      	</td>
                     
                    </tr>
                    @endforeach
                  </tbody>
                  </table>

                </div>
            </div>
    </div>
</div>



@endsection
