@extends('layouts.app')
<style type="text/css">
.path.row{
  margin-bottom:20px;
  height:200px;
}
.path ul li{
  float:right;
  margin-right: 10%;
  font-size:2vw;
  position: relative;
}
.path ul li label{
display:block;
font-size:1vw;
position:absolute;
width:100%;
top:30px;
left:-50px;
cursor: pointer;
}
.path ul li label:hover{
font-size:2rem;
}
@media(max-width:700px)
{
  .path ul li label{
    font-size:1rem;
  }
}
#changes li{
  width:auto;
  padding-left:20px;
}
.selected_change{
  background-color: firebrick;
  color: white;
  border-radius:10px;
  padding:3px;
  padding-right: 15px;
}
</style>

@section('content')

<div class="container">

    <div class="row">
      <div class="col-md-12" style="margin-bottom:20px;">
        <a href="{{url("/Nodes/show/$node->level/$parent")}}">
          <img src="{{asset('/images/pointer_left.png')}}" style="background-color: rgb(48, 151, 209);display: inline;">
        </a>
        <div class="editing alert alert-warning" style="width:100%;height:120px;text-align:center;">
          <h4><span class="glyphicon glyphicon-pencil"></span> Currently Editing users</h4>
          @foreach($editingUsers as $user)
          <div style="display:inline-block;margin-left:20px;">
            <img class="img-circle" style="width:50px;height:50px;" src="{{url($user->photo)}}">
            <label style="display:block;color:red;">{{$user->id==Auth::user()->id?'Me':$user->name}}</label>
          </div>
          @endforeach
        </div>
      
      
      </div>

              <h4 class="alert alert-success review-notify text-center" style="margin-top:20px;"></h4>
              @if(session()->has('notify_revert'))
              <div class="row" style="display:block;text-align:center;">
                @if(session('notify_revert'))
                <h4 class="alert alert-success" style="margin-top:20px;">Revert successfully made !</h4>
                @else
                <h4 class="alert alert-failure" style="margin-top:20px;">Revert failed !</h4>
                @endif
                <?php session()->pull('notify_revert');?>
              </div>
              @endif

        <div class="col-md-12 col-md-offset-0">
            <div class="panel panel-default">

                <div class="panel-heading mb-50"  style="text-align:center;">
                  <div class="pull-left">
                  <a href="{{url("/Reviews/$node->id")}}" >See Reviews</a> |
                  <a href="">older versions</a>
                  </div>

                  <b style="font-size:2rem;">Version: <span id="version">{{$node->current_version}}</span></b>
                </div>

                <div class="col-md-12">
                <div class="pull-right">
                  <button class="btn btn-default" onclick="toggleReadMode();"><img style="width:30px;" src="{{asset('images/read-only.png')}}">
                     Read mode
                  </button>
                  <button class="btn btn-success" onclick="$('.review-form').fadeToggle();"><img style="width:30px;" src="{{asset('images/check.png')}}">
                     Review
                  </button>

                   <button onclick="$('.revert-form').fadeToggle();" class="btn btn-danger btn-lg">Commits <span class="badge">{{$logs_count}}</span>
                   </button>

                  </div>

                  <div class="review-form" style="display:none;margin-top:30px;padding-bottom:20px;">
                  <input id="review" class="form-control" type="text" placeholder="Add summary text here ..." name="" style="display:inline;width:50%;">
                   <button onclick="addNodeReview()" class="btn btn-info" style="margin:10px;"><img style="width:40px;display:inline" src="{{asset('images/save.png')}}" onclick=""> <b> Save</b></button>
                  </div>

                  <div class="revert-form pull-left" style="display:none;margin-top:30px;padding-bottom:20px;">

                  <ul id="changes"  style="font-size:1.5rem;border-radius:10px;margin-left:100px;list-style:disc;" >
                    @foreach($logs as $log)
                    <li data-changeId='{{$log->id}}' class="{{$node->last_commited_log == $log->id ? 'selected_change' : ''}}" style="cursor:pointer;"> {{$log->summary}} - {{$log->time}} by {{Auth::user()->id==$log->user_id?'Me':$log->name}} </li>
                    @endforeach
                  </ul>
                <!--   <ul style="display:block;;">
                     <li id="Oldest">Oldest</li>
                     <canvas id="line"></canvas>
                     <li id="Newest">Newest</li>
                    </ul> -->
                  <form  method="POST" action="{{url('/Nodes/revert')}}" >
                    {{ csrf_field() }}
                    <input type="text" name="nodeId" value="{{$node->id}}" hidden="hidden">
                    <input id="newState" type="hidden" name="newState" value="">
                    <input id="changeId" type="hidden" name="changeIds" value="" >
                    <button id="revert" type="submit" class="btn btn-danger" disabled>
                    <img style="width:20px;" src="{{asset('images/undo.png')}}">
                    Revert
                    </button>
                  </form>
                  </div>
                  </div>

                <div class="panel-body" >

                   {!! Form::open([ 'method' =>'PATCH','id'=>'edit-form' , 'route' =>['Nodes.update', $node->id] ]) !!}

                  <!--  <input type="hidden" name="olderLog" value="$older_log"> -->
                    <div class="form-group">
                   {!! Form::text('name', $node->name, ['required', 'placeholder'=>'node name', 'class'=>'form-control text-center']) !!}
                   </div>

                    <div id="tinyMce" class="form-group">
                   {!! Form::textarea('content', $node->content, [ 'placeholder'=>'node content', 'class'=>'form-control']) !!}
                   </div>
                   <!--i level 1 , parent_id = doc_id-->

                    <div class="form-group">
                  {!! Form::text('message', '', ['required', 'placeholder'=>'commit message', 'class'=>'form-control text-center']) !!}
                    </div>
                    <div class="form-group text-center">
                   {!! Form::submit('Commit', [ 'class'=>'btn btn-primary btn-lg']) !!}
                    </div>
                   {!! Form::close() !!}

                </div>

                <div id='read-div'></div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="{{asset('js/vendor/tinymce/js/tinymce/tinymce.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/tinymce_conf.js')}}"></script>
<script type="text/javascript" src="{{asset('js/revert_list.js')}}"></script>
<script type="text/javascript" src="{{asset('js/current_editing_users.js')}}"></script>


<script type="text/javascript">

// close edits for user (remove user from active editors on the node)
function addNodeReview() {
  var data = {
              "node_id":{{$node->id}},
              "summary":$('#review').val(),
              "_token": $('meta[name="csrf-token"]').attr('content')  
              };

   $.ajax({
      headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
      method:'POST',
      url:"{{url('/Reviews')}}",
      data: JSON.stringify(data),
      contentType:'application/json; charset=utf-8',
      dataType: 'json' 
          
          })
          .done(function(data) {
            if(data.status == 0 )
            {
              $("#version").html(parseInt($("#version").html())+1); 
              $("h4.alert.alert-success.review-notify").html(data.msg);
            }
            //console.log(data);
          })
          .fail( function(jqXHR, textStatus, errorThrown){
            console.log(jqXHR);
          }) ;
}



// close edits for user (remove user from active editors on the node)
window.onbeforeunload = function (event) {
    var data = {
                "node_id":{{$node->id}},
                "_token": $('meta[name="csrf-token"]').attr('content')  
                };

     $.ajax({
        headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                          },
        method:'POST',
        url:"{{url('/UserCloseNode')}}",
        data: JSON.stringify(data),
        contentType:'application/json; charset=utf-8',
        dataType: 'json' 
            
            })
            .done(function (response) {
              //console.log(response.data);
            })
            .fail( function(jqXHR, textStatus, errorThrown){
              //console.log(jqXHR);
            }) ;
      //console.log(window.location.href);
  }




</script>

@endsection
