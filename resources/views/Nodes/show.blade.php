@extends('layouts.app')
<link href="{{ URL::asset('css/nodes.css') }}" rel="stylesheet">
<link href="{{ URL::asset('css/popup.css') }}" rel="stylesheet">
<link href="{{ URL::asset('css/bottom_modal.css') }}" rel="stylesheet">

@section('content')
    <?php  //storing doc id for ajax requests & create url
    $doc_id = $level == 1 ? $parent->id : $parent->doc_id;
    ?>

    <div class="container">
        <div class="row">
            <div class="popup">
                <span class="popupText" >Node actions notifications !</span>
            </div>

            <div class="col-md-1">
                <a href="{{url('/Docs')}}">
                    <img src="{{URL::asset('images/pointer_left.png')}}" style="background-color:#3097D1;"/>
                </a>
            </div>
            <div class="col-md-7 text-left">
              <h3 >
               <span style="border-bottom:1px dashed;font-weight:bold;"> {{ $level == 1 ? $parent->name: $parent->doc->name}}</span></h3>
            </div>
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading">View</div>
                    <div class="panel-body" style="width: 100%;">
                        <div id="preview" style="position:fixed;left:200px;top:20px;width:40%;height:60%;z-index:9999;display:none;background-repeat:no-repeat;"></div>

                            <div class="dropdown" onmouseleave="shown=false;$('#preview').fadeOut();">
                            <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true"><?php if(isset($_GET["view"])&&$_GET["view"]==1) print 'Queue'; else if(isset($_GET["view"])&&$_GET["view"]==2) print 'Row'; else if(isset($_GET["view"])&&$_GET["view"]==2) print 'Circular'; ?>
                            <span class="caret" ></span> view</button>
                            <ul class="dropdown-menu">
                                <li class="<?= isset($_GET["view"])&&$_GET["view"]==1? 'active':'' ?>"   onmouseover="showViewModel(1);"><a href="#" onclick="changeViewPreference(1)">Queue</a></li>
                                <li class="divider"></li>
                                <li class="<?= isset($_GET["view"])&&$_GET["view"]==2|| !isset($_GET["view"])? 'active':'' ?>"  onmouseover="showViewModel(2);"><a href="#" onclick="changeViewPreference(2)" >Row</a></li>
                                <li class="divider"></li>
                                <li class="<?= isset($_GET["view"])&&$_GET["view"]==3? 'active':'' ?>" onmouseover="showViewModel(3);"><a href="#" onclick="changeViewPreference(3)" >Circular</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
        <div class="row">
        
            <div class="row" style="overflow: hidden;">
                <div id="nodes">
                    <h3 style="text-align:center;">
                        Level : {{$level}}
                    </h3>
                    <div class="container1 rotate-animation">
                        <div class="branch">
                        </div>
                        <div class="bubble" >
                            <p style="font-size:2rem;margin-top:40px;">
                                {{$parent->name}}
                            </p>
                        </div>
                    </div>
               
                    <div>
                        <a class="btn btn-primary"  href="{{ url( "Nodes/create/$doc_id/$level/$parent->id" ) }}" style="float:right;font-size:10rem;margin-right:10px;">
                            +
                        </a>
                    </div>
                    <div class="clear">
                    </div>
                    <div  id="children" class="container2" >
                        @if( count($children_nodes)>0)
                          @foreach( $children_nodes as $index=>$node )
                        <div id='node-{{$node->id}}' class="container3">

                            <div class="bubble weight" onclick="animateTopBubble(90);">
                                <p>     <?php $nextLevel = $level+1; ?>
                                    <a href="{{url("Nodes/show/$nextLevel/$node->id")}}">
                                        {{$node->name}}
                                    </a>
                                </p>
                                <a href="{{url('Nodes/'.$node->id.'/edit')}}" style="line-height:50px;" title="go to text editor !">
                                    Edit Content
                                </a>
                            </div>
                            <div class="shadow">
                                <div class="hide-me controls">
                                   <div class="dropup" style="display: inline-block;">
                                      <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown" style="font-size:1.2rem;" >
                                        Assign recursively to
                                      <span class="caret"></span></button>
                                      <ul class="dropdown-menu" >
                                        @if( count($contributers) > 0)
                                        @foreach( $contributers as $index=>$contributer )
                                        <li class="contributer"><a   href="javascript:;">{{$contributer->name}}&nbsp;&nbsp;
                                        <span style="font-size:1.3rem;float:right;" class="glyphicon glyphicon-chevron-right"></span></a>
                                        <div class="chevron-right">
                                        <h4 class="" onclick="assign({{$contributer->id}}, {{$node->id}}, 1)"> Admin</h4>
                                        <h4 onclick="assign({{$contributer->id}}, {{$node->id}}, 2)"> Editor</h4>
                                         <h4 onclick="assign({{$contributer->id}}, {{$node->id}}, 3)"> Reviewer</h4>
                                        </div>
                                        </li>
                                         @endforeach
                                         @else
                                         <li><a href="javascript:;">Empty List !</a></li>
                                         @endif
                                      </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="dropdown" style="display: inline-block;">
                                      <button class="btn dropdown-toggle" type="button" data-toggle="dropdown" style="position:absolute;top:0px;font-size:1.2rem;" >
                                      <span class="glyphicon glyphicon-cog"></span></button>
                                      <ul class="dropdown-menu" >
                                       <li class="cut-copy"><a onclick="showToolTip(event, 'node has been cut !', {{$node->id}})"   href="javascript:;">Cut &nbsp;&nbsp;
                                       </a>
                                        </li>
                                        <li class="cut-copy">
                                        <a onclick="showToolTip(event, 'node has been copied !', {{$node->id}})" href='javascript:;'>Copy</a>
                                        </li>
                                        <li>
                                       <!--  {!! Form::open(array('method'=>'DELETE', 'route'=>['Nodes.destroy', $node->id ], 'style'=>'display:inline;')) !!} -->
                                        <button onclick="showModalDelete({{$node->id}})" type="button" class="btn btn-danger" > Remove</button>
                                        <!-- {!! Form::close() !!}  -->

                                        </li>
                                        </ul>
                                    </div>
                        </div>
                        @if($index< count($children_nodes)-1)
                        <div id='branch-{{$node->id}}' class="branch">
                        </div>
                        @endif
						@endforeach
						@else
                        <h1 style="color:#F0AD4E;text-align: center;">
                            0 Nodes
                        </h1>
                        @endif
                    </div>
                </div>
            </div>
           
            <!-- scroll buttons -->
            @if( count($children_nodes)>0)
             <div id="scrolls" class="row">
                <div style="display:inline;width:100px;font-size:50px;border-radius:50%;background-color:#2579A9;text-align:center;float:left;"  onclick="scroll_left()" >
    			<a href="javascript:;" style="color:white;text-decoration:none;">	&lt;&lt;
    			</a>
    			</div>
                <div style="display:inline;text-align:center;">
                <h4 id="assign-alert" style="margin-top:20px;">Notification</h4>
                </div>
    			<div class="pull-right" style="display:inline;width:100px;font-size:50px;border-radius:50%;background-color:#2579A9;text-align:center;" onclick="scroll_right()" >
    		        <a href="javascript:;"  style="color:white;text-decoration:none;">
    		            &gt;&gt;
    		        </a>
    			</div>
            </div>
            @endif
             <!-- chats -->
             <div class="row">
                 Tree Structure to view all hierarchy of children nodes (click button to show as it's optional), more like tree view in desktop. it's good and fast overview for explanation or tracking of what's going on.
             </div>
        </div>
    </div>
    

    <div class="hidePasteMenu" id="pasteMenu" onclick="paste(event, {{$doc_id}} )">
            <a  href="javascript:;">
                Paste
            </a>
    </div>


  <!--          The bottomModal                 -->
<div id="bottomModal" class="bottomModal">
  <!-- bottomModal content -->
  <div class="bottomModal-content">
    <div class="bottomModal-header">
      <span class="close_bottomModal">&times;</span>
      <h2>Are you sure to remove this Node ?</h2>
    </div>
    <div class="bottomModal-body">
      <button  class="btn btn-danger">Yes</button>
      &nbsp;&nbsp;&nbsp;
      <button class="btn btn-link" onclick="$('#bottomModal').slideToggle();">No</button>
    </div>
    <div class="bottomModal-footer">
      <h3></h3>
    </div>
  </div>
</div>
@endsection

<script type="text/javascript" src="{{ URL::asset('js/bottom_modal_nodes.js') }}"></script>

<script type="text/javascript">
var shown = false;

function showViewModel(index) {
  var url ="/DocBuilder/public/images/nodes/"+ index +".png" ;
  $('#preview').css('background-image', "url("+url+")");

  if(!shown)
  { 
    shown = true;
    $('#preview').fadeIn();
  }
}

function changeViewPreference(index){
    localStorage.setItem('nodes-view', index);
    // window.location.reload();
    window.location.href = window.location.pathname+"?"+$.param({'view':index})

}

function deleteNode(NodeId)
{
    var data = {
                 // "_token": $( this ).find( 'input[name=_token]' ).val(),
                  "node_id": NodeId,
                  "_token": $('meta[name="csrf-token"]').attr('content')
              };


    $.ajax({
        headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
      method:'DELETE',
      url:"{{url('/Nodes')}}",
      data: JSON.stringify(data),
      contentType:'application/json; charset=utf-8',
      dataType: 'json' 
          
          } ).done(function (response) {
           console.log(response);
            if (response.status == 0) {
                  // remove node from html
                 
                $('#node-'+NodeId).detach();
                $('#branch-'+NodeId).detach();

                $('#assign-alert').removeClass();//clear class list
                $('#assign-alert').addClass('alert-success');
                $('#assign-alert').html(response.msg);
                $('#assign-alert').fadeIn();
            }
          }).fail( function(jqXHR, textStatus, errorThrown){
              console.log(jqXHR);
              }) ;
   
}

function assign(assignedUserId, NodeId, perm)
{
    var data = {
                 // "_token": $( this ).find( 'input[name=_token]' ).val(),
                  "user_id": assignedUserId,
                  "doc_id": {{$doc_id}},
                  "node_id": NodeId,
                  "allDoc":false,
                  "permission":perm,
                  "_token": $('meta[name="csrf-token"]').attr('content')
              };


    $.ajax({
        headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
      method:'POST',
      url:"{{url('/Permissions')}}",
      data: JSON.stringify(data),
      contentType:'application/json; charset=utf-8',
      dataType: 'json' 
          
          } ).done(function (response) {
            if (response.status == 0) {

                $('#assign-alert').removeClass();//clear class list
                $('#assign-alert').addClass('alert-success');
                $('#assign-alert').html('assignment added successfully !');
                $('#assign-alert').fadeIn();
            }
            else if (response.status == 1) {
                $('#assign-alert').removeClass();//clear class list
                $('#assign-alert').addClass('alert-warning');
                $('#assign-alert').html('assignment already exists !');
                $('#assign-alert').fadeIn();
            }
            else if (response.status == 2) {
                $('#assign-alert').removeClass();//clear class list
                $('#assign-alert').addClass('alert-danger');
                $('#assign-alert').html('user not registered on the app !');
                $('#assign-alert').fadeIn();
            }
            else if (response.status == 4) {
                $('#assign-alert').removeClass();//clear class list
                $('#assign-alert').addClass('alert-danger');
                $('#assign-alert').html('can not register this on the list !');
                $('#assign-alert').fadeIn();
            }

          }).fail( function(jqXHR, textStatus, errorThrown){
              console.log(jqXHR);
              }) ;
   
  }
 

function animateTopBubble(value)
{
    $('.container1').toggleClass('animation');
}

function scroll_left() {
//console.log($('#nodes').offset());//Get the current coordinates of the first element, or set the coordinates of every element, in the set of matched elements, relative to the document.
	$('#children').animate({scrollLeft: document.getElementById('children').scrollLeft - 280}, 'fast');
	//document.getElementById('children').scrollLeft-= 170;
}

function scroll_right() {
	$('#children').animate({scrollLeft: document.getElementById('children').scrollLeft + 280}, 'fast');
}



// copy paste logic
function paste(event, docId ) {

    $.ajax({
        headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
      method:'GET',
      url:"{{url('/pasteNode')}}/"+localStorage.nodeId+"/"+docId+"/"+{{ $level==1? 0 : $parent->id}},
      contentType:'application/json; charset=utf-8',
      dataType: 'json' 
          } ).done(function (response) {
            if (response.status == 0) {
                //clear cache
                localStorage.nodeId = 0 ;
                //report progress
               location.reload();
            }
            else if (response.status == 1) {
                
            }

          }).fail( function(jqXHR, textStatus, errorThrown){
            console.log(jqXHR);
    }) ;


}

function showToolTip(evt, text, nodeId)
{   //update session with ajax or use this simple storage with encryption
    localStorage.setItem("nodeId", nodeId);
    //sessionStorage.setItem("nodeId", nodeId);
    $('.popup').css({top:mouseY(evt)-30 ,left:mouseX(evt)+50 });
    $('.popupText').text(text);
    $('.popup').fadeIn('slow');

    setTimeout(function () {
        $('.popup').fadeOut('slow');
    }, 1000);
}

function mouseX(evt) {
    if (evt.pageX) {
        return evt.pageX;
    } else if (evt.clientX) {
       return evt.clientX + (document.documentElement.scrollLeft ?
           document.documentElement.scrollLeft :
           document.body.scrollLeft);
    } else {
        return null;
    }
}

function mouseY(evt) {
    if (evt.pageY) {
        return evt.pageY;
    } else if (evt.clientY) {
       return evt.clientY + (document.documentElement.scrollTop ?
       document.documentElement.scrollTop :
       document.body.scrollTop);
    } else {
        return null;
    }
}

document.addEventListener('DOMContentLoaded', function(){ 

//  disable paste if empty cache
if(localStorage.nodeId == 0  || localStorage.nodeId == undefined)
{
   document.getElementById("pasteMenu").style.display = 'none'; 
}
//  bind on click for doc to hide pate on any click
$(document).bind("click", function(event) {
    document.getElementById("pasteMenu").className = "hidePasteMenu";

});
document.getElementById('nodes').addEventListener('contextmenu', function(event){
    window.event.returnValue = false;
     if(!event.target.matches('.bubble') && !event.target.matches('p') && !event.target.matches('a')) {
    document.getElementById("pasteMenu").className = "showPasteMenu";  
        document.getElementById("pasteMenu").style.top =  mouseY(event) + 'px';
        document.getElementById("pasteMenu").style.left = mouseX(event) + 'px';
        }
});


function  checkIfPageNeedScrollButtons()
{
    var children = document.getElementById('children');
    var hasHorizontalScrollbar = children.scrollWidth > children.clientWidth;
    console.log(hasHorizontalScrollbar);
    if (hasHorizontalScrollbar) {
        $('#scrolls').fadeIn();
    }
   
}

checkIfPageNeedScrollButtons();


}, false);

 </script>
