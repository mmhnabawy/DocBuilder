@extends('layouts.app')

<style type="text/css">
    

</style>
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Create Node</div>

                <div class="panel-body">
                   
                   {!! Form::open([ 'route' =>'Nodes.store' ]) !!}
                   
                   <!-- hint : doc_id field  not used in controller -->
                    {!! Form::text('doc_id',  $info['doc'], [ 'hidden']) !!}
                     {!! Form::text('level', $info['level'], [  'hidden']) !!}
                     {!! Form::text('parent_id', $info['parent_id'], [  'hidden']) !!}

                    <div class="form-group">
                   {!! Form::text('name', null, ['required', 'placeholder'=>'node name', 'class'=>'form-control']) !!}
                   </div>
                   
                    <div class="form-group">
                   {!! Form::textarea('content', null, [ 'placeholder'=>'node content', 'class'=>'form-control']) !!}
                   </div>
                   <!--i level 1 , parent_id = doc_id-->
                    <div class="form-group">
                   {!! Form::submit('add', [ 'class'=>'btn btn-info']) !!}
                    </div>
                   {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
</div>



@endsection
