@extends('layouts.app')
<style type="text/css">
.logs{
    line-height: 30px;
} 
.logs:hover{
background-color: #ddd;
border-radius: 5px;
}
</style>
@section('content')

<link href="{{URL::asset('css/home.css')}}" rel="stylesheet" type="text/css">
    <div class="container" >
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3>
                                Contribution Activity
                            </h3>
                        </div>
                        <div class="panel-body well well-lg">
                            <h4 class="well">Recent Commits</h4>
                            <hr>
                            <ul>
                                @if(count($commits)==0)
                                <li>
                                    <label class="text-warning">There are no commits yet !
                                    </label>
                                </li>
                                @else
                                @foreach($commits as $commit)
                                <li>
                                    <p class="logs">
                                        <a href="{{url('$commit->node->doc->id')}}">
                                        {{$commit->user->name}}
                                        </a>
                                        made an edit to the node
                                        <a href="{{url('Nodes/'.$commit->node->id.'/edit')}}">
                                            {{$commit->node->name}}
                                        </a>
                                        in th document
                                        <a href="{{url('Nodes/show/1/'.$commit->node->doc->id)}}">
                                           {{$commit->node->doc->name}}
                                        </a>
                                         {{$commit->time}} .
                                    </p>
                                 <!--    <p style="background-color:orange;color:white;display:inline;">
                                        first commit
                                    </p>
                                    <a href="javascript:;" style="background-color:#222222;color:white;font-size:2rem;border-radius:50%;width:100px;height:50px;padding: 10px;" title="show summary !">
                                        <i class="glyphicon glyphicon-eye-open">
                                        </i>
                                    </a> -->
                                </li>
                                @endforeach
                            </ul>
                            <a href="">See Commits History</a>
                            @endif
                            <p> Limitations : 20 level, each level contains 20 nodes for free, tree overview, changes tracking algorithm

                            ProTip! The feed shows you events from people you follow and repositories you watch. Subscribe to your news feed</p>
                           
                        </div>
                        <hr>
                        <div class="panel-body well well-lg">
                            <h4 class="well">Recent Reviews</h4>
                            <hr>
                            <ul>
                                @if(count($reviews)==0)
                                <li>
                                    <label class="text-warning">There is no review yet !
                                    </label>
                                </li>
                                @else
                                @foreach($reviews as $review)
                                <li>
                                    <p class="logs">
                                        <a href="{{url('$review->node->doc->id')}}">
                                        {{$review->user->name}}
                                        </a>
                                        made a review on the node
                                        <a href="{{url('/Reviews/'.$review->node->id)}}">
                                            {{$review->node->name}}
                                        </a>
                                        in th document
                                        <a href="{{url('Nodes/show/1/'.$review->node->doc->id)}}">
                                           {{$review->node->doc->name}}
                                        </a>
                                         {{$review->time}} .
                                    </p>
                                    <!-- <p style="background-color:orange;color:white;display:inline;">
                                        first review
                                    </p>
                                    <a href="javascript:;" style="background-color:#222222;color:white;font-size:2rem;border-radius:50%;width:100px;height:50px;padding: 10px;" title="show summary !">
                                        <i class="glyphicon glyphicon-eye-open">
                                        </i>
                                    </a> -->
                                </li>
                                @endforeach
                                <a href="">See Reviews History</a>
                                @endif
                            </ul>
                            
                        </div>
                    </div>

                    <div class="panel-body">
                        @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                        @endif
                 
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="sun">
        <div class="dot">
        </div>
    </div>
    ​

@endsection
</link>